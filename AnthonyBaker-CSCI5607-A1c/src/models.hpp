#ifndef MODELS_HPP
#define MODELS_HPP





/**
 * This file will hold the classes derived from Model which are actual things to be drawn and shoul implement the Model functions
 */






/// STL
#include <cmath>
#include <vector>

/// Project
#include "entity.hpp"
#include "entities.hpp"
#include "model.hpp"
#include "util.hpp"
#include "vec.hpp"









class Sphere : public Model
{
	public:
	Sphere(){};
	void print() override;
	bool hit(const Ray&, float&);
	vec3 get_normal(const vec3&) const;	
	Bounding_box bounding_box(){return Bounding_box();};
	vec2 get_uv(const vec3& p);

	float r=0;
};

void Sphere::print()
{
	std::cout << "Sphere: " << std::endl;
	std::cout << '\t' << "Name: " << name << std::endl;
	std::cout << '\t' << " Origin: " << origin.to_str() << std::endl;
	std::cout << '\t' << " Radius: " << r  << std::endl;
	std::cout << '\t' << " Material (od,os,ka,kd,ks,n,alpha,eta) : (" << material.Od.to_str() << "," << material.Os.to_str() << "," << material.ka << "," << material.kd << "," << material.ks << "," << material.n << "," << material.alpha << "," << material.eta << ")" << std::endl;	
	std::cout << '\t' << "is_textured: " << is_textured << std::endl; 	
}





bool Sphere::hit(const Ray& ray, float& t)
{

	float A=0.0;
	float B=0.0;
	float C=0.0;
	float discriminant=0;

	float ts1=0; // used in the 2 solution case
	float ts2=0; // used in the 2 solution case

	//A = std::pow(ray.direction.x,2) + std::pow(ray.direction.y,2) + std::pow(ray.direction.z,2) ; // non-normalized direction version
	A = 1;		// Because this is normalized, A=1  // Don't use this if ray is not normalized.
	B = 2* (ray.direction.x*(ray.origin.x - origin.x)  + ray.direction.y*(ray.origin.y - origin.y)  + ray.direction.z*(ray.origin.z - origin.z)); 
	C = pow((ray.origin.x - origin.x),2) + pow((ray.origin.y - origin.y),2)  + pow((ray.origin.z - origin.z),2)  -  pow(r,2.f);;

	
	// compute discriminant B^2 - 4AC
	discriminant = (B*B) - (4*A*C);
	
	if ( discriminant < 0 ) // no solution
	{return false;}

	else if ( std::fabs(discriminant) <= FLT_EPSILONe04 )	// 1 solution 	
	{
		// parameter value of intersection
		t = (-B)/(2*A); // discriminant zero

		if(t>0)  // In front of eye
			return true;
	}

	else if ( discriminant > 0 ) // two solutions - 
	{
		// Which t value is closer to eye?
		ts1 = ((-B) + (float)sqrt(discriminant)) /(2.f*A);			
		ts2 = ((-B) - (float)sqrt(discriminant)) /(2.f*A);

		if( (ts1 >0) && (ts1<ts2) )
			{t = ts1;}
		else if(ts2>0 && ts2<ts1)
			{t =ts2;}

		//if(t>0) // In front of eye
		return true;
	}

	
	return false; // dummy return
}




vec3 Sphere::get_normal(const vec3& p) const
{
	vec3 temp;
	// for a sphere the normal at a point p is
	temp = (p - origin)/r;
	return temp; // normalize(temp); // I don't think normalize is necessary here
}


vec2 Sphere::get_uv(const vec3& p)
{
	vec2 uv;



	/* lesson time


	x = origin.x + r cos θ sin φ
	y = origin.y + r sin θ sin φ
	z = origin.z + r cos φ

	*/

/*
	It is possible to align the poles of the texture with
	a different axis by shuffling the roles of N x , N y ,
	and N z ; for example, setting: 
	φ = acos(N x ), 
	θ =	atan2(N z , N y ) or 
	
	φ = acos(N y ), 
	θ = atan2(N x , N z )

*/

	// axis alignment 1
	//float theta = std::atan2( get_normal(p).y, get_normal(p).x );
	//float phi = std::acos( get_normal(p).z );	// acos in [0, PI]	

	// axis alignment 2
	//float theta = std::atan2( get_normal(p).z, get_normal(p).y );
	//float phi = std::acos( get_normal(p).x );	// acos in [0, PI]	

		// rotations
		float theta = std::atan2( get_normal(p).z, get_normal(p).y );
		float phi = std::acos( -get_normal(p).x );	// acos in [0, PI]		
	
	
	
	// axis alignment 3
	//float theta = std::atan2( get_normal(p).x, get_normal(p).z );
	//float phi = std::acos( get_normal(p).y );	// acos in [0, PI]	
	

	
	if(theta <0)
		theta += 2*PI;

	uv.x = theta/(2*PI);//u
	uv.y = phi/PI;		//v

	return uv;

	
	
}
	
//Box Sphere::bounding_box(){return Box();};













/*
class Ellipsoid : public Model
{
	public:
	Ellipsoid(){};
	void print(){};
	bool hit(const Ray&, float&);
	vec3 get_normal(const vec3&) const;	
	Bounding_box bounding_box(){return Bounding_box();};
		
	// transition this to base class origin		
	float x=0;
	float y=0;
	float z=0;
	float rx=0;
	float ry=0;
	float rz=0;

};
bool Ellipsoid::hit(const Ray& ray, float& t)
{

	return false;
}


vec3 Ellipsoid::get_normal(const vec3& p) const
{
	vec3 temp;
	// Define this
	//temp = (p - origin)/r;
	return temp;
}
*/










class Tri : public Model
{
	public:
	Tri(){};
	void print();
	bool hit(const Ray&, float&) override;
	vec3 get_normal(const vec3& p) const; // get normal of plane coinciding with triangle
	Bounding_box bounding_box(){return Bounding_box();};
	vec2 get_uv(const vec3& p) override;
	
	
	// vertices
	vec3 p0;
	vec3 p1;
	vec3 p2;
	
	
	// normal at each vertex
	vec3 n0_idx;
	vec3 n1_idx;
	vec3 n2_idx;
	
	
	// normal in center of tri
	vec3 n;

	// texture coordinates
	vec2 uv0;
	vec2 uv1;
	vec2 uv2;
	
	// a place to hold barycentric coordinates for points
	float alpha=666;
	float beta=0;
	float gamma=0;

};




void Tri::print()
{
	std::cout << "Tri: " << std::endl;
	std::cout << '\t' << "p0: (" << p0.x << "," << p0.y << "," << p0.z << ")" << std::endl;
	std::cout << '\t' << "p1: (" << p1.x << "," << p1.y << "," << p1.z << ")" << std::endl;
	std::cout << '\t' << "p2: (" << p2.x << "," << p2.y << "," << p2.z << ")" << std::endl;

	std::cout << '\t' << " Material (od,os,ka,kd,ks,n,alpha,eta) : (" << material.Od.to_str() << "," << material.Os.to_str() << "," << material.ka << "," << material.kd << "," << material.ks << "," << material.n << "," << material.alpha << "," << material.eta << ")" << std::endl;	
	std::cout << '\t' << "is_textured: " << is_textured << std::endl; 
	std::cout << '\t' << "barycentrics: (" << alpha << "," << beta << "," << gamma << ")" << std::endl;
	std::cout << '\t' << "this : " << this << std::endl;
}


	
bool Tri::hit(const Ray& ray, float& t)
{

	
	// Compute the prerequisites:
	vec3 e1= p1-p0;
	vec3 e2= p2-p0;
	n= cross(e1,e2); // this is the class' normal n


	// solve the plane equation Ax+By+Cz+D = 0 for D first
	float D = -1*( (n.x*p0.x) + (n.y*p0.y) + (n.z*p0.z) );

	// check denominator first when solving for t
	float denominator = (n.x*ray.direction.x) + (n.y*ray.direction.y) + (n.z*ray.direction.z);
	if( (denominator < FLT_EPSILONe04) && (denominator > -1.f*FLT_EPSILONe04) ) // zero with error // if( (denominator < epsilon) && (denominator > -1*epsilon) ) // zero with error
		return false;

	float numerator = -1*(n.x*ray.origin.x + n.y*ray.origin.y + n.z*ray.origin.z +D);
	
	// solve for t
	t = numerator/denominator;

	if( t <0) // behind the eye // do the epsilon thing here?
		return false;

	vec3 p = ray.origin + (ray.direction*t);


	// compute Area of total triangle
	float A = norm(n)*0.5f;



	// can we somehow avoid creating all these temp variables every time?

	// compute area of subtriangles created by p
	// we need e1 x e2 for the subtriangles
	vec3 e3 = (p-p1);
	vec3 e4 = (p-p2);
	vec3 na= cross(e3,e4);
	vec3 nb= cross(e4,e2);
	vec3 nc= cross(e1,e3);
			
	float a = norm(na)*0.5f;
	float b = norm(nb)*0.5f;
	float c = norm(nc)*0.5f;

	alpha=a/A;
	beta=b/A;
	gamma=c/A;

	// check bounds on alpha, beta, gamma for being in interior


	if(alpha+beta+gamma -1 > FLT_EPSILONe04) // Point not in triangle
		return false;

	if( (alpha < 0) || (alpha >1) )
		return false;	
	if( (beta < 0) || (beta > 1) )
		return false;
	if( (gamma < 0) || (gamma >1) )
		return false;	

	return true;
}




vec3 Tri::get_normal(const vec3& p) const
{
	// doesn't matter where the point p is. we do it by the plane
	vec3 e1 = p1-p0;
	vec3 e2 = p2-p0;
	vec3 temp_n = normalize(cross(e1,e2));
	return temp_n;
}


vec2 Tri::get_uv(const vec3& p)
{
	
	vec2 uv;
	
		// compute u,v for our particular intersection point		
	uv.x = (uv0.x * alpha) +
			(uv1.x * beta)  +
			(uv2.x * gamma);

	uv.y = (uv0.y * alpha) +
			(uv1.y * beta)  +
			(uv2.y * gamma);


	return uv;
}














class Face
{
	public:
	Face(){};
	void print();

	bool is_textured = 0 ;
	bool is_smooth = 0;
	// index into verts
	int v1=0;
	int v2=0;
	int v3=0;

	// index into texture coordinates
	int uv0_idx=0;
	int uv1_idx=0;
	int uv2_idx=0;

	// index into normals
	int n0_idx=0;
	int n1_idx=0;
	int n2_idx=0;
};
void Face::print()
{
	std:: cout << "Face: " << std::endl;
	std::cout<< '\t' << "Vertex indices: (" << v1 << "," << v2 << "," << v3 << ")" << std::endl;
	std::cout<< '\t' << "Texture coord indices: (" << uv0_idx << "," << uv1_idx << "," << uv2_idx << ")" << std::endl;
	std::cout<< '\t' << "Normal indices: (" << n0_idx << "," << n1_idx << "," << n2_idx << ")" << std::endl;		

}



/*
class Mesh : public Model
{
	public:
	Mesh(){};
	void print(){};
	bool hit(const Ray&, float&);
	vec3 get_normal(const vec3&) const;			
	Bounding_box bounding_box(){return Bounding_box();};
				
	std::vector<Tri> tris;
	std::vector<Face> faces;
	std::vector<vec3> normals;
};
bool Mesh::hit(const Ray& ray, float& t)
{

	return false;
}

vec3 Mesh::get_normal(const vec3& p) const
{
	vec3 temp;
	// define this 
	// temp = (p - origin)/r;
	return temp;
}
*/















#endif
