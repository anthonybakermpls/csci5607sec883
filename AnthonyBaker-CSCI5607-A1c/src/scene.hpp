#ifndef SCENE_HPP
#define SCENE_HPP




class Scene 
{
	public:
	
	Scene(){};

	void print();

	
	

	vec3 bg_color;
	int max_color = 255; // 0 to 256-1
	
	bool enable_lighting=1;
	bool enable_shadow=1;
	
	//std::vector<Sphere> sphere_v;
	//std::vector<Ellipsoid> ellipsoid_v;

	std::vector<Light> light_v;
	std::vector<Spotlight> spotlight_v;

	std::vector<vec3> vertices_v;
	std::vector<vec3> normals_v;
	std::vector<vec2> tex_coords_v;	
	std::vector<int> face_vertices_v;
	std::vector<Tri> tris_v;

	std::vector<std::unique_ptr<Model>> model_uptr_v;

	std::string texture_dir = "./";
	std::map<std::string,PPM> textures_stdmap;
	//std::vector<Texture> texture_v;

};




void Scene::print()
{

	std::cout << "bg_color: "
	<< bg_color.to_str();

	std::cout << '\n';

	std::cout << "Texture dir: " << texture_dir << std::endl;



	std::cout << "Light list: " << '\n';
	for(auto x: light_v)
		x.print();
	for(auto x: spotlight_v)
		x.print();

	std::cout << "Vertices: " << '\n';
	for(auto x: vertices_v)
		std::cout << '\t' << x.to_str() << '\n';
		
	std::cout << "Faces: " << '\n';
	for(auto x: face_vertices_v)
		std::cout << '\t' << x << '\n';


	for(unsigned int i =0; i< model_uptr_v.size(); i++)
	{
		std::cout << "model_uptr_v.at(" << i << ") : " ;
		model_uptr_v.at(i)->print();
	}

	std::cout << "Texture files: " << '\n';
	for(auto x: textures_stdmap)
		std::cout << x.first << std::endl;

}









std::unique_ptr<Tri> create_triangle_uptr(const Scene& scene, const Face& f)  // pass in array of verts and a face index
{
	// our faces describe the triangles

	auto tr = std::unique_ptr<Tri>{new Tri()};

	tr->p0 = scene.vertices_v.at(f.v1);
	tr->p1 = scene.vertices_v.at(f.v2);
	tr->p2 = scene.vertices_v.at(f.v3);

	if(f.is_textured)
	{
		tr->is_textured=1;
		tr->uv0 = scene.tex_coords_v.at(f.uv0_idx);
		tr->uv1 = scene.tex_coords_v.at(f.uv1_idx);
		tr->uv2 = scene.tex_coords_v.at(f.uv2_idx);
	}
	
	if(f.is_smooth)
	{
		tr->is_smooth=1;
		tr->n0_idx = scene.normals_v.at(f.n0_idx);
		tr->n1_idx = scene.normals_v.at(f.n1_idx);
		tr->n2_idx = scene.normals_v.at(f.n2_idx);
	}


	return tr;
	//return std::move(tr); // We don't do this here see online for why
}




#endif
