
/**
 * This program (executable name: assignment1b) reads in an ascii text file 
 * which contains a specified input consisting of a keywords specifing 
 * various properties of the viewing space and output image.
 * The program then generates and writes a file in the required 
 * PPM format with the dimensions as read in from the input file.
 *
 * The basic steps are:
 * - define viewing parameters
 * - define the scene
 * - define equation of each ray and
 * - determine if/where a ray intersects an object
 * 
 * 
 * Usage:
 * ./assignment1c inputfile.txt
 * ./assignment1c ../inputfile.txt; display ../input.ppm
 * 
 * /home/anthony/Data/Code/CSCI5607SEC883/AnthonyBaker-CSCI5607-A1c/input.txt /home/anthony/Data/Code/CSCI5607SEC883/AnthonyBaker-CSCI5607-A1c/
 *./assignment1d ./input_simple.txt;display ./input_simple.ppm
 *./assignment1d ./assign1b_0.txt; ./assignment1d ./assign1b_1.txt; ./assignment1d ./assign1b_2.txt; display ./assign1b_0.ppm; display ./assign1b_1.ppm; display ./assign1b_2.ppm
 *
 * Notes:
 * - Base class had Material and the derived class had Material member variables. This caused problems when setting derived class member values but then referencing through unique_ptr used the base class values.,
 * - Back up further in the execution if something doesn't make sense.
 * - 	another case of this: variables change because some other function that happens in between where you're looking
 */



// std
#include <algorithm> //min max
#include <chrono>
#include <complex>
#include <cmath>
#include <exception>
#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <random> // random number device
#include <vector>

// project
#include "entity.hpp"
#include "io.hpp"
#include "parm_obj.hpp"
#include "models.hpp"
#include "scene.hpp"
#include "util.hpp"





// Function declarations
void compute_viewing_parms(Parm_obj&);

void compute_pixels(Parm_obj&, Scene& scene);
void create_viewing_ray(Parm_obj&, Ray&, const int& i, const int& j);


vec3 get_texel(Scene&, int, const vec3& );
uint compute_shadow_factor(const Scene&, const int&, const vec3& );
vec3 trace_ray(Parm_obj&, const Ray&, Scene& scene);
vec3 shade_ray(Parm_obj&, const Ray&, Scene& scene, const float&, const int&);





int main( int argc, char **argv )
{
	// Timer for measuring performance
	auto start = std::chrono::steady_clock::now();


	try
	{
		// Make sure the user specified an input file. If not, tell them how to do so.
		if( argc < 2 ) // if passing texture dir on command line use 3 )
		{
			throw std::runtime_error ("No input file or texture directory provided. Usage: e.g. \"./example ../examplefile.txt\" Note that textures are assumed to be in CWD.");
		}

		Parm_obj parm_obj; // Contains many of our variables and is used for passing through functions. Refactor when necessary
		IO io; // Handles reading input files and filling parameters
		Scene scene;
		
		std::string input_filename=argv[1];
		// scene.texture_dir = argv[2]; // Assume that texture is in CWD

		io.read_scene_file(input_filename, parm_obj, scene); // Read input file
		compute_viewing_parms(parm_obj);
		//parm_obj.print();
		//scene.print();

		compute_pixels(parm_obj,scene);

		std::string output_filename = input_filename.substr(0, input_filename.size() - 4) + ".ppm";	// filename is something like blah.txt and we want it to be blah.ppm
		io.write_ppm(output_filename, parm_obj, scene);
		

	}
	catch (std::exception& e) 
	{
		std::cerr << "Exception: " << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	catch (const std::string& s) 
	{
		std::cerr << "Exception: " << s << std::endl;
		exit(EXIT_FAILURE);
	}
	//catch (std::ios_base::failure& e)
	//{
	//	std::cerr << "failbit | badbit" << std::endl;
	//	//exit(EXIT_FAILURE);		
	//}	
    catch(...) // Catch anything else.
    {
		std::cerr << "General exception." << std::endl;
		exit(EXIT_FAILURE);
    }


	// Timer for measuring performance
	auto end = std::chrono::steady_clock::now();			
	auto diff = end - start;
	std::cout << "Program duration: " << (std::chrono::duration <double, std::ratio<60>> (diff).count()) << " min" << std::endl;

	return 0;
}




















/** 
 * This function takes care of setting up all viewing parameters that are
 * not given in our input file
 **/
void compute_viewing_parms(Parm_obj& parm_obj)
{
	
	// compute u
	parm_obj.u = normalize( cross(parm_obj.view_dir,parm_obj.up_dir) );

	// compute v
	parm_obj.v = normalize( cross(parm_obj.u, parm_obj.view_dir) );

	// compute n
	parm_obj.n= normalize(parm_obj.view_dir);

	// compute w
	parm_obj.w= parm_obj.n*(-1.f); // oppopse of n or the view direction

	
	// compute viewing window w,h in world coordinate units
	// parm_obj.view_w = 2*distance* (tan(fovh/2)); // do this below based off of h and aspect
	parm_obj.view_height =  2.f * (parm_obj.distance) *  (float)tan( (parm_obj.fovv/2.f) * (PI/180.0f) ) ;	// CMATH TAN IS IN RADIANS

	
	// compute aspect ratio given image/view window size
	parm_obj.aspect= (float)parm_obj.imsize_w/(float)parm_obj.imsize_h;


	if(std::fabs(parm_obj.aspect- 1) <= FLT_EPSILON)
	{
		//std::cout << "Aspect ratio is 1." << std::endl;
		parm_obj.view_width=parm_obj.aspect*parm_obj.view_height;
	}
	
	// compute corners of viewing window in world coordinate space
	parm_obj.ul =  (parm_obj.eye + parm_obj.n*parm_obj.distance) + parm_obj.v* (float)parm_obj.view_height/2.f - parm_obj.u* (float)parm_obj.view_width/2.f;
	parm_obj.ur =  (parm_obj.eye + parm_obj.n*parm_obj.distance) + parm_obj.v* (float)parm_obj.view_height/2.f + parm_obj.u* (float)parm_obj.view_width/2.f;
	parm_obj.ll =  (parm_obj.eye + parm_obj.n*parm_obj.distance) - parm_obj.v* (float)parm_obj.view_height/2.f - parm_obj.u* (float)parm_obj.view_width/2.f;
	parm_obj.lr =  (parm_obj.eye + parm_obj.n*parm_obj.distance) - parm_obj.v* (float)parm_obj.view_height/2.f + parm_obj.u* (float)parm_obj.view_width/2.f;

	// compute delta  h,v (map from image size to view window)
	parm_obj.delta_h= (parm_obj.ur - parm_obj.ul)/((float)parm_obj.imsize_w-1);
	parm_obj.delta_v= (parm_obj.ll - parm_obj.ul)/((float)parm_obj.imsize_h-1);
	
}




















void create_viewing_ray(Parm_obj& parm_obj, Ray& ray, const int& i, const int& j)
{
	// ray equation is r(t) = e + t(p - e)
	vec3 p;
	vec3 p_minus_e;
	
	ray.origin = parm_obj.eye;
	p = parm_obj.ul + (parm_obj.delta_h*(float)i) + (parm_obj.delta_v*(float)j);
	p_minus_e = p - ray.origin;
	
	ray.direction= normalize(p_minus_e); // normalize
}





uint compute_shadow_factor(const Scene& scene, const int& i, const vec3& intersection )
{

	Ray shadow_ray;
	vec3 shadow_ray_intersection; // intersection between shadow ray and object in scene
	uint shadow_factor=1;
	float shadow_t=0;

	shadow_ray.origin = intersection; // add an epsilon to this in direction of ray


	// Directional light source
	if (scene.light_v.at(i).w == 0) 
	{
		shadow_ray.direction = (scene.light_v.at(i).origin) * -1; // directional light stores it's direction in the origin variables.
		for (unsigned int i2 = 0; i2 < scene.model_uptr_v.size(); i2++) // check all in scene
		{
			if ( scene.model_uptr_v.at(i2)->hit( shadow_ray, shadow_t) )
			{
				shadow_factor =0; // In shadow: set factor to 0 which will cancel color values
			}
		}
	}

	// point/ positional
	else if (scene.light_v.at(i).w ==1) 
	{
		//shadow_ray.direction = normalize(scene.light_v.at(i).origin - intersection); // note this is different for the directional light
		shadow_ray.direction = normalize(scene.light_v.at(i).origin); // note this is different for the directional light		
		for (unsigned int i2 = 0; i2 < scene.model_uptr_v.size(); i2++) // check all in scene
		{
			if ( scene.model_uptr_v.at(i2)->hit( shadow_ray, shadow_t) )
			{
				if(shadow_t <1) // If it hit we also know it will be > 0 
				shadow_factor =0; // In shadow: set factor to 0 which will cancel color values
			}
		}
	}

	return shadow_factor;
}





vec3 get_texel(Scene& scene, int closest_index, const vec3& intersection)
{
	vec3 temp_Od;

	int ppm_w = 0;
	int ppm_h = 0;
	int max_color =0;
	int uv_x_scaled   = 0; 
	int uv_y_scaled   = 0;  
	int ppm_data_vector_location;
	vec2 uv;

	
	// get texture coordinate at p.
	uv = scene.model_uptr_v.at(closest_index)->get_uv(intersection);


	
	// Map texture coordinate to our PPM file.
	
	// Get texture dimensions
	ppm_w = scene.textures_stdmap.at(scene.model_uptr_v.at(closest_index)->texture_name).w;
	ppm_h = scene.textures_stdmap.at(scene.model_uptr_v.at(closest_index)->texture_name).h;

	// Scale the uv coordinates to match our texture image size
	uv_x_scaled  = round(uv.x* (float)(ppm_h -1) ); // NOTE x texture coordinate goes vertically from top to bottom
	uv_y_scaled = round(uv.y* (float)(ppm_w -1) ); // y texture coordinate goes horizontally left to right
	

	// Now we map from uv to coordinates in ppm file.
	// PPM coordinates are serialized in a std::vector. Start at upper left and move to the right (y) then restart on next row (x) 
	ppm_data_vector_location = (uv_x_scaled*ppm_w*3) + (uv_y_scaled*3);


	// copy this in just for debugging
	//PPM ppm = scene.textures_stdmap.at(scene.model_uptr_v.at(closest_index)->texture_name);

	// get the colors from PPM file
	temp_Od.x =	scene.textures_stdmap.at(scene.model_uptr_v.at(closest_index)->texture_name).data.at( ppm_data_vector_location );
	temp_Od.y =	scene.textures_stdmap.at(scene.model_uptr_v.at(closest_index)->texture_name).data.at( ppm_data_vector_location +1);
	temp_Od.z =	scene.textures_stdmap.at(scene.model_uptr_v.at(closest_index)->texture_name).data.at( ppm_data_vector_location +2 );

	
	max_color = scene.textures_stdmap.at(scene.model_uptr_v.at(closest_index)->texture_name).max_color;	
	
	// translate our PPM int colors back to the range [0,1]	
	temp_Od.x= normalize_color(temp_Od.x,max_color);
	temp_Od.y= normalize_color(temp_Od.y,max_color);
	temp_Od.z= normalize_color(temp_Od.z,max_color);

	// set the material diffuse to be what we just calculated
	scene.model_uptr_v.at(closest_index)->material.Od.x = temp_Od.x;
	scene.model_uptr_v.at(closest_index)->material.Od.y = temp_Od.y;
	scene.model_uptr_v.at(closest_index)->material.Od.z = temp_Od.z;
	
	return temp_Od;

}


/**
 * For each object in our scene, check to see if the given ray intersects it.
 */
vec3 trace_ray(Parm_obj& parm_obj, Ray& ray, Scene& scene)
{
	vec3 return_color = scene.bg_color;

	float min_t=99999; // this is the minimum t out of all "hits" we will overwrite on the pixel if current t is less than this	
	float t = min_t; // reset this for our new ray

	// Use a vector of unique pointers to base class Model
	int closest_index=0;
	for (unsigned int i = 0; i < scene.model_uptr_v.size(); i++)	// check all Models in the scene
	{
		if ( scene.model_uptr_v.at(i)->hit( ray,t) )
		{
			if(t < min_t)
			{
				min_t = t;
				closest_index=i;
			}
		}
	}

	if(min_t < 99999) // there was a hit in our range
		return_color = shade_ray(parm_obj, ray, scene, min_t, closest_index); // 

	return return_color;
}




vec3 shade_ray(Parm_obj& parm_obj, const Ray& ray,  Scene& scene, const float& t, const int& closest_index)
{


	vec3 return_color;


	if( !scene.enable_lighting ) // if lighting is turned off return the ambient
	{
		return_color.x = scene.model_uptr_v.at(closest_index)->material.ka * scene.model_uptr_v.at(closest_index)->material.Od.x;
		return_color.y = scene.model_uptr_v.at(closest_index)->material.ka * scene.model_uptr_v.at(closest_index)->material.Od.y;
		return_color.z = scene.model_uptr_v.at(closest_index)->material.ka * scene.model_uptr_v.at(closest_index)->material.Od.z;
		return return_color;
	}


	vec3 V; // to eye
	vec3 L; // to light
	vec3 N; // surface normal
	vec3 H; // half way between light L and eye V ( v+l/(norm(v+l)) )
	
	float Ir=0;
	float Ig=0;
	float Ib=0;
	float N_dot_L=0;
	float N_dot_H=0;
	float N_dot_H_To_Nth=0;
	uint shadow_factor=1; // we use uint because bool would be misleading. a zero means in shadow and is used as a multiplication factor.
	vec3 intersection;

	/* future use
	float Fr=0;
	float F0=0;
	vec3 I;
	Ray Iray;	
	Ray R;
	vec3 Rlamb; // Illumination returned by reflection ray R		
	*/

	//intersection = ray.origin + (ray.direction - ray.origin)*t ; // point at which our ray intersects with object
	intersection = ray.origin + (ray.direction)*t ; // point at which our ray intersects with object

	// Determine surface normal N
	N = scene.model_uptr_v.at(closest_index)->get_normal(intersection); 

	// Determine the direction to the viewer V
	V = normalize(parm_obj.eye - intersection);

	/* future use
	// Direction of incoming ray:
	Iray.origin = parm_obj.eye;
	Iray.direction = (normalize(intersection - parm_obj.eye)*-1);
	*/
	

	for(unsigned int i=0; i< scene.light_v.size(); i++)
	{
		shadow_factor = 1; // reset to a factor of 1
		
		// Determine L
		if (scene.light_v.at(i).w == 0) // Directional light source (the origin is used to hold the direction components)
			L = normalize(scene.light_v.at(i).origin *(-1));

			// THIS IS BROKEN SOMEHOW
		else if (scene.light_v.at(i).w ==1) // point/ positional 
			L = normalize(scene.light_v.at(i).origin - intersection);

		// Determine the direction of the halfway vector H
		H = normalize(L + V);

		N_dot_L = std::max( 0.f, dot(N, L));
		N_dot_H = std::max( 0.f, dot(N, H));
		N_dot_H_To_Nth = (float) pow( N_dot_H, scene.model_uptr_v.at(closest_index)->material.n);


		vec3 kd_od;
		vec3 ks_os;
		
		ks_os = scene.model_uptr_v.at(closest_index)->material.Os * scene.model_uptr_v.at(closest_index)->material.ks;


		if( ! (scene.model_uptr_v.at(closest_index)->is_textured) ) // 0 or 1
		{
			// copy in the basic material
			kd_od = scene.model_uptr_v.at(closest_index)->material.Od * scene.model_uptr_v.at(closest_index)->material.kd;
		}
		else // Otherwise we look up diffuse in texture map per pixel
		{ 
			// Important thing to note - For triangles this relies on our barycentric coordinates that are currently set in the object to the current
			// intersection point. Anytime we call hit() those will change

			// replace Od with our texel
			vec3 temp_Od;
			temp_Od = get_texel(scene,closest_index,intersection);	// set the diffuse color based on that retrieved value		
			kd_od = temp_Od; // kd isn't used here
		}


		/* future use
		// recursively find reflection color
		R.origin = intersection;
		float a=dot(N,I);		
		R.direction = (N-I)*2*a;
		Rlamb = trace_ray(parm_obj, R, scene);
		*/




		/** Shadow computations 	
		**************************/	
		//shadow_factor = compute_shadow_factor(scene, i, intersection); 
		//shadow_flag = 1; // shadows are broken so turn them off for now	
		
		//compute the values componentwise			
		Ir= Ir + (float)shadow_factor* scene.light_v.at(i).intensity.x * ( (kd_od.x *  N_dot_L) + (ks_os.y *  N_dot_H_To_Nth) );	
		Ig= Ig + (float)shadow_factor* scene.light_v.at(i).intensity.y * ( (kd_od.y *  N_dot_L) + (ks_os.y *  N_dot_H_To_Nth) );	
		Ib= Ib + (float)shadow_factor* scene.light_v.at(i).intensity.z * ( (kd_od.z *  N_dot_L) + (ks_os.y *  N_dot_H_To_Nth) );

	}



	/* future use
	// Add Fresnel and R
	F0 = pow(((scene.model_uptr_v.at(closest_index)->material.eta-1)/(scene.model_uptr_v.at(closest_index)->material.eta+1)),2);
	I = Iray.direction;
	Fr = F0 + (1-F0)* pow((1- dot(I,N)),5);
	Ir = Ir + (Rlamb.x * Fr);
	Ig = Ig + (Rlamb.y * Fr);
	Ib = Ib + (Rlamb.z * Fr);	
	*/
	
	// Add ambient terms
	Ir = Ir + (scene.model_uptr_v.at(closest_index)->material.ka * scene.model_uptr_v.at(closest_index)->material.Od.x);
	Ig = Ig + (scene.model_uptr_v.at(closest_index)->material.ka * scene.model_uptr_v.at(closest_index)->material.Od.y);
	Ib = Ib + (scene.model_uptr_v.at(closest_index)->material.ka * scene.model_uptr_v.at(closest_index)->material.Od.z);

	return_color.x = clamp(Ir,0.f,1.f);
	return_color.y = clamp(Ig,0.f,1.f);
	return_color.z = clamp(Ib,0.f,1.f);
	
	return return_color;
}















/**
 * for each pixel
 * Compute ray equation
 * compute viewing ray
 * find 1st object hit by ray and its surface normal n
 * set pixel color to value computed from hit point, light, and n
 */
void compute_pixels(Parm_obj& parm_obj, Scene& scene)
{
	/// TODO: Change so we don't push the entire bg and then overwrite - do each pixel only once.

	// push back the full image worth of rgb zeros

	for(int i=0; i< parm_obj.imsize_w*parm_obj.imsize_h; i++)// 3 rgb values (in 0-max_color range) per pixel
	{ 
		parm_obj.pixel_values.push_back( denormalize_color(scene.bg_color.x , scene.max_color ) ); 
		parm_obj.pixel_values.push_back( denormalize_color(scene.bg_color.y , scene.max_color ) ); 
		parm_obj.pixel_values.push_back( denormalize_color(scene.bg_color.z , scene.max_color ) ); 
	}
	
	// use i as horizontal image pixel dimension and j as vertical image pixel dimension
	// but in row-major order so i is in innner loop...
	
	int vec_element=0; // used for std::vector element location
	vec3 color;
	Ray ray; // our primary viewing ray that we'll reuse

	for(int j=0; j<parm_obj.imsize_h; j++) //  move down
	{	
		for (int i =0; i<parm_obj.imsize_w; i++) //  move across
		{
	
			//i=300;
			//j=300;
			
			create_viewing_ray(parm_obj, ray, i,j);
			color = trace_ray(parm_obj, ray, scene); // do the ray/object intersection and pixel value storage

			// we are at pixel i,j. This translates to location in a serial vector element list of
			vec_element = (j*parm_obj.imsize_w + i )* 3u; // multiply by three because we have 3 values per pixel
			
			// Set vector elements. Need to denormalize our colors to parm_obj.max_color	
			parm_obj.pixel_values.at(vec_element) =  denormalize_color(color.x,scene.max_color);
			parm_obj.pixel_values.at(vec_element+1u) =  denormalize_color(color.y,scene.max_color);
			parm_obj.pixel_values.at(vec_element+2u) =  denormalize_color(color.z,scene.max_color);
		}
	}

}





































