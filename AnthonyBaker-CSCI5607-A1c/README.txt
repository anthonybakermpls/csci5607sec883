﻿Anthony Baker
CSCI 5607 SEC 883

Assignment 1c:
This program demonstrates the use of textures.




To build:

mkdir build;
cd build;
cmake ..;
make;



Usage:
./assignment1c input.txt

Note! The textures are assumed to be in the same dir as the executable per TA instruction.




Directories:
	scenes_bak: This is where I'm storing all of my scene files and textures.
	src: Source code directory
	Tests: At one time I foolishly thought I would have time to create and run automated tests and some of those scripts for that are here.
