eye 0 0	 10
viewdir 0 0 -1
updir 0 1 0
fovv 45
imsize 600 600
bkgcolor 0.2 0.2 0.3


light 4  5 0 1 1 1 1
light 1 -1 0 0 1 1 1


# REFERENCE GREEN
mtlcolor 0 1 0 1 1 1   0.2 0.8 0.0 100
sphere -4 -3 -5 0.8

#SUN
mtlcolor 1 1 0 1 1 1 0.5 0.5 1 32
sphere -10 10 -20 1.8


# column going "up"
# Odr Odg Odb Osr Osg Osb ka kd ks n
mtlcolor 0 0 1 1 1 1 1 1 0 8
sphere -5 -4 -20 0.3
sphere -5 -3 -20 0.3
sphere -5 -2 -20 0.3
sphere -5 -1 -20 0.3
sphere -5  0 -20 0.3
sphere -5  1 -20 0.3
sphere -5  2 -20 0.3
sphere -5  3 -20 0.3
sphere -5  4 -20 0.3
sphere -5  5 -20 0.3
sphere -5  6 -20 0.3
sphere -5  7 -20 0.3


# ROW GOING "FORWARD"
mtlcolor 1 0 0 1 1 1 0.2 0.6 0.8 10
sphere -1 -1 -0   0.3
sphere -1 -1 -10  0.3
sphere -1 -1 -20  0.3
sphere -1 -1 -30 0.3
sphere -1 -1 -40 0.3
sphere -1 -1 -50 0.3
sphere -1 -1 -60 0.3
sphere -1 -1 -70 0.3
sphere -1 -1 -80 0.3
sphere -1 -1 -90 0.3
sphere -1 -1 -100 0.3
sphere -1 -1 -110 0.3



# REFERENCE TRI
# 220,11,145
mtlcolor 0.862 0.043 0.568 1 1 1 1 0.6 0.8 10
v -1 0 0
v 1 0 0
v 0 1 0
f 1 2 3




# EARTH SPHERE
# Odr Odg Odb Osr Osg Osb ka kd ks n
mtlcolor 0 0 0 0.5 0.5 0.5 0.2 0.6 0.2 6
texture earth.ppm
sphere 3 0 -2 2




mtlcolor 1 1 1 0 0 0 0 0 0 0
texture at.ppm
# quad
v -2 0 5
v -1 0 5
v -1 1 3
v -2 1 3
vt 1 0
vt 1 1
vt 0 1
vt 0 0
f 4/1 5/2 6/3
f 4/1 6/3 7/4




# Test with normals
#v 0 0 0
#v -1 1 0
#v -1 0 0
#vt 1 0
#vt 1 1
#vt 0 1
#vn 1 1 1
#vn 1 1 1
#vn 1 1 1
#f 4/1/1 5/2/2 6/3/3







