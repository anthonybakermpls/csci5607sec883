#version 330
in vec4 vertex_position;
in vec4 vertex_color;
out vec4 fragment_color;
	


void main()  
{
	gl_Position = vertex_position;  // pass on the vertex position unchanged
	fragment_color = vertex_color;
}
