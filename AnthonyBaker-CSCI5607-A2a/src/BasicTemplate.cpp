// Basic OpenGL program
// Based on example code from: Interactive Computer Graphics: A Top-Down Approach with Shader-Based OpenGL (6th Edition), by Ed Angel


/*
 * Notes
 * in/out variables that pass data between vertex and frag shader must be the same
 */


#define GLFW_INCLUDE_GLCOREARB



#include <cmath>
#include <complex>
#include <vector>




#include <GL/glew.h>

#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>

// This file contains the code that reads the shaders from their files and compiles them
#include "ShaderStuff.hpp"





// function that is called whenever an error occurs
static void
error_callback(int error, const char* description){
    fputs(description, stderr);  // write the error description to stderr
}

// function that is called whenever a keyboard event occurs; defines how keyboard input will be handled
static void
key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) // checks to see if the escape key was pressed
        glfwSetWindowShouldClose(window, GL_TRUE);  // closes the window
}

// initialize some basic structures and geometry
typedef struct {
    float x, y;
} FloatType2D;

int nvertices = 4;



void fractal(const int&, const int&, std::vector<float>&, const int&);
void attractor(std::vector<float>&);



//void init(const int& w, const int& h, const std::vector<float>& verts_v)
void init(const int& w, const int& h, const std::vector<float>& colors_v)
{

    GLuint vao[1];
    glGenVertexArrays( 1, vao );
    glBindVertexArray( vao[0] );


	GLuint vertex_buffer_id;
	GLuint location;
	GLuint location2;
	GLuint shader_program;
	GLuint color_buffer_id;



    //vertices[0].x = -0.9;  vertices[0].y = -0.9;
    //vertices[1].x =  0.9;  vertices[1].y = -0.6;
    //vertices[2].x =  0.9;  vertices[2].y =  0.6;
    //vertices[3].x = -0.9;  vertices[3].y =  0.9;

	/** mandelbrot */
	// Need a vector of w x h vertices that matches with our color array/vector
	FloatType2D pix;
	std::vector<FloatType2D> verts_v;
    for(int y=0; y<h; y++)  // for each row
    {
        for(int x=0; x<w; x++) //  each column
        { 
			// have to normalize coordinates [-1,1]
			pix.x= ( (float)x-(w/2) )/(float)(w/2); //   shift and normalize
			pix.y= ( (float)y-(h/2) )/(float)(h/2); //   shift and normalize
			verts_v.push_back(pix);
		}
	}
    std::cout << "Generated vertices data of size " << "(" << w << "," << h << ") vector size: " << verts_v.size() << std::endl;

	// Global variable used in main  glDrawArrays
	nvertices=verts_v.size();

    glGenBuffers( 1, &vertex_buffer_id );
    glBindBuffer( GL_ARRAY_BUFFER, vertex_buffer_id );
    glBufferData( GL_ARRAY_BUFFER, verts_v.size() * sizeof(FloatType2D), &verts_v[0], GL_STATIC_DRAW );

	glGenBuffers(1, &color_buffer_id);
	glBindBuffer(GL_ARRAY_BUFFER, color_buffer_id);
	glBufferData(GL_ARRAY_BUFFER, colors_v.size() * sizeof(float),  &colors_v[0], GL_STATIC_DRAW);


	/** Attractor */
	//nvertices=verts_v.size();
    //glGenBuffers( 1, &vertex_buffer_id );
    //glBindBuffer( GL_ARRAY_BUFFER, vertex_buffer_id );
    //glBufferData( GL_ARRAY_BUFFER, verts_v.size() * sizeof(float), &verts_v[0], GL_STATIC_DRAW );
	
	
	
	
	
	
    // Define the names of the shader files
    std::stringstream vshader, fshader;
    vshader << SRC_DIR << "/vshader_frac.glsl";
    fshader << SRC_DIR << "/fshader_frac.glsl";

    // Load the shaders and use the resulting shader program
    shader_program = InitShader( vshader.str().c_str(), fshader.str().c_str() );


    location = glGetAttribLocation( shader_program, "vertex_position" );
    glEnableVertexAttribArray( location );
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_id);		
    glVertexAttribPointer( location, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );

	
	// 2nd attribute buffer : colors
    location2 = glGetAttribLocation( shader_program, "vertex_color" );
	glEnableVertexAttribArray( location2 );
	glBindBuffer(GL_ARRAY_BUFFER, color_buffer_id);	
	glVertexAttribPointer(location2,4,GL_FLOAT,GL_FALSE,0,(GLvoid*) (0));




    // Define OpenGL state variables
    glClearColor( 0.2, 0.2, 0.3, 1.0 ); // white background
	
	// This is for transparency
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Need to enable multisampling with your context.
	glEnable(GL_MULTISAMPLE);

    glPointSize(1.0);
    //glLineWidth(5.0);  // fat lines
}




























int main(void){

	GLFWwindow* window;

	int w=800;
	int h=600;
// 	int w=2560;
// 	int h=1440;

	
	

	std::vector<float> verts_v;
	
	/** generate Mandelbrot */	
	fractal(w, h, verts_v, 255);
	
	/** generate Attractor */	
	//attractor(verts_v);
	
	

    // Define the error callback function (one of the few things that can precede glfwInit)
    glfwSetErrorCallback(error_callback);

    // Initialize GLFW (performs platform-specific initialization)
    if (!glfwInit()) exit(EXIT_FAILURE);

    // Ask for OpenGL 3.2
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_SAMPLES,16);
    // Use GLFW to open a window within which to display your graphics
	window = glfwCreateWindow(w, h, "Basic Test", NULL, NULL);
	
    // Verify that the window was successfully created; if not, print error message and terminate
    if (!window)
	{
        printf("GLFW failed to create window; terminating\n");
        glfwTerminate();
        exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window); // makes the newly-created context current
	glfwSwapInterval(1);  // tells the system to wait to swap buffers until monitor refresh has completed; necessary to avoid tearing

    // Define the keyboard callback function
    glfwSetKeyCallback(window, key_callback);

	
	glewExperimental = GL_TRUE; 
	glewInit();




	// Create the shaders
	init(w,h, verts_v);




	while (!glfwWindowShouldClose(window)) {

        // draw graphics
		glClear( GL_COLOR_BUFFER_BIT );     // fills the window with the background color

		glDrawArrays( GL_POINTS, 0, nvertices );    // draw a point at each vertex location
		//glDrawArrays( GL_LINES, 0, nvertices );    // draw a line between each vertex pair
		//glDrawArrays( GL_LINE_LOOP, 0, nvertices );    // draw a line between each successive vertex
		//glDrawArrays( GL_TRIANGLES, 0, nvertices );    // draw a triangle for each vertex triple
		//glDrawArrays( GL_TRIANGLE_FAN, 0, nvertices );    // draw a triangle between the first vertex and each successive vertex pair
		glFlush();	

        glfwSwapBuffers(window);  // swap buffers
        glfwPollEvents();  // check for events

	} // end graphics loop

	// Clean up
	glfwDestroyWindow(window);
	glfwTerminate();  // destroys any remaining objects, frees resources allocated by GLFW
	exit(EXIT_SUCCESS);

} // end main







// http://nathanselikoff.com/training/tutorial-strange-attractors-in-c-and-opengl
// Note he uses GLUT and immediate mode. We are going to fill a vbo
void attractor(std::vector<float>& verts_v)
{
	float	x=0.1, y=0.1, 
			a=-0.9676918,
			b=2.879879,
			c=0.765145,
			d=0.744728;



	int initial_iterations = 100,
		iterations = 100000;	// In c++14 we can do 100'000
		
	// compute some initial iterations to settle into the orbit of the attractor
	for (int i = 0; i < initial_iterations; i++) 
	{
		// compute a new point using the strange attractor equations
		float xnew = sin(y*b) + c*sin(x*b);
		float ynew = sin(x*a) + d*sin(y*a);
	
		// save the new point
		x = xnew;
		y = ynew;
	}
	
	// go through the equations many times, drawing a point for each iteration
	for (int i = 0; i < iterations; i++) 
	{
 
		// compute a new point using the strange attractor equations
		float xnew = sin(y*b) + c*sin(x*b);
		float ynew = sin(x*a) + d*sin(y*a);
 
		// save the new point
		x = xnew;
		y = ynew;
 
		// draw the new point
		verts_v.push_back(x);
		verts_v.push_back(y);		
	}

}







//
void fractal(const int &w, const int &h, std::vector<float>& pixel_values, const int& max_color)
{

    std::cout << "Generating fractal dataset. Please be patient." << std::endl;


	// create a mapping from [x,y] to [a,b] for our image size to mandelbrot radius
	// I had to look this up because i thought i could use a scalar and that didn't work
	// (val - x)*(b-a)/(y-x) + a
	int a=-2;
	int b=2;
	float b_minus_a = b-a;

    float x=0;
    float y=0;
    float epsilon = 1; //0.01; // The step size across the X and Y axis

    int max_iterations = 1000; // increasing this will give you a more detailed fractal

    std::complex<double> Z;
    std::complex<double> C;
	float r=0; // temp real value
	float im=0; // temp imaginary value

    int iterations=0;

	float red = 0;
	float green=0;
	float blue=0;
	float alpha=1;


//	bool first_calc=1; // for time estimation code
	
    for(y=0; y<h; y+= epsilon)  // for each row
    {
		//std::cout << "Computing next row: " << y << '\n';
        for(x=0; x<w; x+= epsilon) //  each column
        {
//			if(first_calc==1)
//				auto start = chrono::steady_clock::now();
			
			//std::cout << "Computing next column: " << x << '\n';
            iterations = 0;
			r=(x)*(b_minus_a)/(w)+a;
			im=(y)*(b_minus_a)/(h)+a;
            C = std::complex<double>(r,im);
            Z = std::complex<double>(0,0);
            while( (std::abs(Z) < 2) && (iterations < max_iterations) )
            {
                Z = Z*Z + C;
                iterations++;
            }

			// messing around with color schemes
			//red=((iterations*-max_color)/max_iterations) +max_color; // linear mapping'
			//green= 200*( 1/(1+exp(-1*iterations)) ); // sigmoid
			//blue=255*log10(iterations); // log mapping
			//blue = ((iterations-0)*(max_color))/(max_iterations);   // linear mapping from [0,max_iterations] to [0,max_color]

			red= ( (iterations*(-1))/max_iterations ) +1; // linear mapping from [0,max_iterations] to [max_color,0]

			if(iterations>=max_iterations)
			{
				green=0;
				blue=0;
			}
			else
			{
				//green= 200*( 1/(1+exp(-1*iterations)) ); // sigmoid;
				blue=log10(iterations); // log mapping 
			}

			//std::cout << "rgb: " << red << "," << green << "," << blue << std::endl;

			// Store RGB components for each pixel (normalize)
			pixel_values.push_back( (red) * 0.4);
			pixel_values.push_back( green );
			pixel_values.push_back( blue );
			pixel_values.push_back(alpha);


/*	TODO : understand std::chrono
			if(first_calc==1)
			{
				auto end = chrono::steady_clock::now();			
				auto diff = end - start;
				cout << "Estimated time remaining: " << (chrono::duration <double, std::ratio<60>> (diff).count()) *(epsilon*w)*(epsilon*h) << " min" << endl;
				first_calc=0;
			}
*/			
        }
    }
    
    std::cout << "Generated fractal data of size " << "(" << w << "," << h << ") vector size: " << pixel_values.size() << std::endl;
}

