
// The loaders are included by glfw3 (glcorearb.h) if we are not using glew.
#ifdef USE_GLEW
#include <GL/glew.h>
#endif
#include <GLFW/glfw3.h>

// Includes
#include "trimesh.hpp"
#include "shader.hpp"
#include <cstring> // memcpy

// Constants
#define WIN_WIDTH 500
#define WIN_HEIGHT 500

GLdouble PI = 4.0*atan(1.0);









class Mat4x4 {
public:

	float m[16];

	Mat4x4(){ // Default: Identity
		m[0] = 1.f;  m[4] = 0.f;  m[8]  = 0.f;  m[12] = 0.f;
		m[1] = 0.f;  m[5] = 1.f;  m[9]  = 0.f;  m[13] = 0.f;
		m[2] = 0.f;  m[6] = 0.f;  m[10] = 1.f;  m[14] = 0.f;
		m[3] = 0.f;  m[7] = 0.f;  m[11] = 0.f;  m[15] = 1.f;
	}

	void make_identity(){
		m[0] = 1.f;  m[4] = 0.f;  m[8]  = 0.f;  m[12] = 0.f;
		m[1] = 0.f;  m[5] = 1.f;  m[9]  = 0.f;  m[13] = 0.f;
		m[2] = 0.f;  m[6] = 0.f;  m[10] = 1.f;  m[14] = 0.f;
		m[3] = 0.f;  m[7] = 0.f;  m[11] = 0.f;  m[15] = 1.f;
	}

	void print(){
		std::cout << m[0] << ' ' <<  m[4] << ' ' <<  m[8]  << ' ' <<  m[12] << "\n";
		std::cout << m[1] << ' ' <<   m[5] << ' ' <<  m[9]  << ' ' <<   m[13] << "\n";
		std::cout << m[2] << ' ' <<   m[6] << ' ' <<  m[10] << ' ' <<   m[14] << "\n";
		std::cout << m[3] << ' ' <<   m[7] << ' ' <<  m[11] << ' ' <<   m[15] << "\n";
	}

	void make_scale(float x, float y, float z){
		make_identity();
		m[0] = x; m[5] = y; m[10] = x;
	}
};

static inline const Vec3f operator*(const Mat4x4 &m, const Vec3f &v){
	Vec3f r( m.m[0]*v[0]+m.m[4]*v[1]+m.m[8]*v[2],
		m.m[1]*v[0]+m.m[5]*v[1]+m.m[9]*v[2],
		m.m[2]*v[0]+m.m[6]*v[1]+m.m[10]*v[2] );
	return r;
}


//
//	Global state variables
//
namespace Globals {
	double cursorX, cursorY; // cursor positions
	float win_width, win_height; // window size
	float aspect;
	GLuint verts_vbo[1], colors_vbo[1], normals_vbo[1], faces_ibo[1], tris_vao;
	TriMesh mesh;

	//  Model, view and projection matrices, initialized to the identity
	Mat4x4 model;
	Mat4x4 view;
	Mat4x4 projection;

	// user input
	GLdouble mouse_x, mouse_y;
	GLdouble prev_mouse_x, prev_mouse_y;
	GLdouble mouse_dx, mouse_dy;
	bool key_up=false;
	bool key_down=false;
	bool key_left=false;
	bool key_right = false;
	bool alt_key; // save this state accross callbacks
	bool mouse_button_left;
	bool lmb_pressed;	

	// Translation and rotation increments and variables
	float translation_factor=0.05;	
	float translation;
	float rotation_factor=0.02;
	Vec3f rotation_angle; // hold rotations for all axes
    Mat4x4 rotation_matrix;

	//Viewport parameters
	float left=-1;
	float right=1;
	float bottom=-1;
	float top =1;
	float near= 1;
	float far = 10;

	// Eye coordinate system
	Vec3f eye(-18.894,-14,0.0930117); //	Vec3f eye(0,-14,0);
	Vec3f view_dir(0,0,1);
	Vec3f up(0,1,0);
	Vec3f n;
	Vec3f u;
	Vec3f v;
	Vec3f d;
}








// Translation matrix we know and love
void translate(Mat4x4 &m, const Vec3f &v)
{
	m.m[12]=v.data[0];
	m.m[13]=v.data[1];
	m.m[14]=v.data[2];
}

// Create a rotation matrix around one of the 3 axes. We're only using 1 in this program ever.
void rotate(Mat4x4 &m, const Vec3f &rotation_angle)
{
	float x=(rotation_angle.data[0]*PI)/180;
	m.m[5]=cos(x);
	m.m[9]=-sin(x);
	m.m[6]=sin(x);
	m.m[10]=cos(x);

	float y=(rotation_angle.data[1]*PI)/180;
	m.m[0]=cos(y);
	m.m[8]=sin(y);
	m.m[2]=-sin(y);
	m.m[15]=cos(y);

	float z=(rotation_angle.data[1]*PI)/180;
	m.m[0]=cos(z);
	m.m[4]=-sin(z);
	m.m[1]=sin(z);
	m.m[5]=cos(z);
	//print_m(rotate_m);

}



// Update the view matrix using the formulas given.
void update_view_matrix()
{
	using namespace Globals;

	
	// Set up the rotation matrix and apply it first.
	rotation_matrix.m[0] = cos(rotation_angle.data[1]);
	rotation_matrix.m[2] = -sin(rotation_angle.data[1]);
	rotation_matrix.m[8] = sin(rotation_angle.data[1]);
	rotation_matrix.m[10]= cos(rotation_angle.data[1]);

	view_dir = rotation_matrix*view_dir;

	
	//  Derive local coordinates
    n = view_dir;
	n.normalize();
	n = n* -1.f;

	u = up.cross(n);
	v = n.cross(u);

	
	// Apply translation to eye
	eye += (  view_dir * translation); //	
	
	
	// Assign d and then apply all values to the view matrix
	
	d.data[0] = -1*eye.dot(u);
	d.data[1] = -1*eye.dot(v);
	d.data[2] = -1*eye.dot(n);	

	view.m[0] = u.data[0];
	view.m[1] = v.data[0];
	view.m[2] = n.data[0];

	view.m[4] = u.data[1];
	view.m[5] = v.data[1];
	view.m[6] = n.data[1];

	view.m[8] = u.data[2];
	view.m[9] = v.data[2];
	view.m[10] =n.data[2];

	view.m[12] = d.data[0];
	view.m[13] = d.data[1];
	view.m[14] = d.data[2];
}


// Set up the projection matrix according to the formulas given
void update_projection_matrix()
{
	using namespace Globals;
	
	projection.m[0]=(2*near)/(right-left);
	projection.m[5]=(2*near)/(top-bottom);
	projection.m[8]=(right+left)/(right-left);
	projection.m[9]=(top+bottom)/(top-bottom);
	projection.m[10]=(-far+near)/(far-near);
	projection.m[11]=-1;
	projection.m[14]=(-2*near)/(far-near);
	projection.m[15]=0;	
}




//
//	Callbacks
//
static void error_callback(int error, const char* description){ fprintf(stderr, "Error: %s\n", description); }

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
	// Close on escape or Q
	if( action == GLFW_PRESS )
	{
		switch ( key ) 
		{
			case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GL_TRUE); break;
			case GLFW_KEY_Q: glfwSetWindowShouldClose(window, GL_TRUE); break;

            // Update the viewing transformation matrices elsewhere. In this function we are just setting flags for the keys
		
			case 'r':
			case 'R':
				//reset();
				break;

			case 263: // left arrow
				// ROTATE LEFT (COUNTERCLOCKWISE)
				Globals::key_left=true;				
				break;

			case 262: // right arrow
				// ROTATE RIGHT (CLOCKWISE)
				Globals::key_right=true;				
				break;	
		
			case GLFW_KEY_UP: // up arrow
				Globals::key_up=true;	// FORWARD
				break;	
				
			case GLFW_KEY_DOWN: //BACK

				Globals::key_down=true;			
				break;
			
			case 342: // left alt
				if(action = GLFW_PRESS)
					Globals::alt_key=true;
				else if(action = GLFW_RELEASE)
					Globals::alt_key=false;
				break;
		}
	}
	if( action == GLFW_RELEASE )
	{
		switch ( key ) 
		{
			case 263: // left arrow
				// ROTATE LEFT (COUNTERCLOCKWISE)
				Globals::key_left=false;				
				break;

			case 262: // right arrow
				// ROTATE RIGHT (CLOCKWISE)
				Globals::key_right=false;				
				break;	
		
			case GLFW_KEY_UP:
// 				// FORWARD
				Globals::key_up=false;
				break;	
				
			case GLFW_KEY_DOWN: // down arrow
				//BACK
				Globals::key_down=false;
				break;
		}
	}
}


static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	Globals::win_width = float(width);
	Globals::win_height = float(height);
    Globals::aspect = Globals::win_width/Globals::win_height;
	
    glViewport(0,0,width,height);

	// Update the perspective matrix according to the new window size
 	Globals::left = -1*Globals::aspect;
 	Globals::right = Globals::aspect;
	update_projection_matrix();

}


// Function to set up geometry
void init_scene();


//
//	Main
//
int main(int argc, char *argv[])
{

	// Load the mesh
	std::stringstream obj_file; obj_file << MY_DATA_DIR << "sibenik/sibenik.obj";
	if( !Globals::mesh.load_obj( obj_file.str() ) ){ return 0; }
	Globals::mesh.print_details();

	// Scale to fit in (-1,1): a temporary measure to allow the entire model to be visible
    // Should be replaced by the use of an appropriate projection matrix
    // Original model dimensions: center = (0,0,0); height: 30.6; length: 40.3; width: 17.0
//     float min, max, scale;
//     min = Globals::mesh.vertices[0][0]; max = Globals::mesh.vertices[0][0];
// 	for( int i=0; i<Globals::mesh.vertices.size(); ++i ){
//         if (Globals::mesh.vertices[i][0] < min) min = Globals::mesh.vertices[i][0];
//         else if (Globals::mesh.vertices[i][0] > max) max = Globals::mesh.vertices[i][0];
//         if (Globals::mesh.vertices[i][1] < min) min = Globals::mesh.vertices[i][1];
//         else if (Globals::mesh.vertices[i][1] > max) max = Globals::mesh.vertices[i][1];
//         if (Globals::mesh.vertices[i][2] < min) min = Globals::mesh.vertices[i][2];
//         else if (Globals::mesh.vertices[i][2] > max) max = Globals::mesh.vertices[i][2];
//     }
//     if (min < 0) min = -min;
//     if (max > min) scale = 1/max; else scale = 1/min;
//     Mat4x4 mscale; mscale.make_scale( scale, scale, scale );
// 	for( int i=0; i<Globals::mesh.vertices.size(); ++i ){
//         Globals::mesh.vertices[i] = mscale*Globals::mesh.vertices[i];
//     }

	// Set up window
	GLFWwindow* window;
	glfwSetErrorCallback(&error_callback);

	// Initialize the window
	if( !glfwInit() ){ return EXIT_FAILURE; }

	// Ask for OpenGL 3.2
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	
	glfwWindowHint(GLFW_SAMPLES, 32);//16
	
	// Create the glfw window
	Globals::win_width = WIN_WIDTH;
	Globals::win_height = WIN_HEIGHT;
	window = glfwCreateWindow(int(Globals::win_width), int(Globals::win_height), "HW2c", NULL, NULL);
	if( !window ){ glfwTerminate(); return EXIT_FAILURE; }

	// Bind callbacks to the window
	glfwSetKeyCallback(window, &key_callback);
	glfwSetFramebufferSizeCallback(window, &framebuffer_size_callback);

	// Make current
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	// Initialize glew AFTER the context creation and before loading the shader.
	// Note we need to use experimental because we're using a modern version of opengl.
	#ifdef USE_GLEW
		glewExperimental = GL_TRUE;
		glewInit();
	#endif

	// Initialize the shader (which uses glew, so we need to init that first).
	// MY_SRC_DIR is a define that was set in CMakeLists.txt which gives
	// the full path to this project's src/ directory.
	mcl::Shader shader;
	std::stringstream ss; ss << MY_SRC_DIR << "shader.";
	shader.init_from_files( ss.str()+"vert", ss.str()+"frag" );

	// Initialize the scene
	// IMPORTANT: Only call after gl context has been created
	init_scene();
	framebuffer_size_callback(window, int(Globals::win_width), int(Globals::win_height)); 

	// Initialize OpenGL
	glEnable(GL_DEPTH_TEST);
	glClearColor(1.f,1.f,1.f,1.f);

	// Enable the shader, this allows us to set uniforms and attributes
	shader.enable();

	// DO THIS IN init()
    // Initialize the eye position (set at origin for now; probably want it to start elsewhere)
	//Vec3f eye = Vec3f(0.f,0.f,0.f);

	// Bind buffers
	glBindVertexArray(Globals::tris_vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Globals::faces_ibo[0]);

	// Game loop
	while( !glfwWindowShouldClose(window) ){

	

		

		// If a key flag is set then increment one of the data elements and apply it to our view.
	
		if(Globals::key_up)
		{
			Globals::translation = Globals::translation_factor;
			update_view_matrix();
			Globals::translation=0;		
			std::cout << "Globals::eye: (" << Globals::eye.data[0] << "," << Globals::eye.data[1] << "," << Globals::eye.data[2] << ")" << std::endl;						
		}
		
		if(Globals::key_down)
		{
			Globals::translation  = -Globals::translation_factor;
			update_view_matrix();
			Globals::translation=0;
		}
			
		if(Globals::key_right)
		{
			Globals::rotation_angle.data[1] = -Globals::rotation_factor; // y
			//std::cout << "Globals::rotation_angle.data[1]: " << Globals::rotation_angle.data[1] << std::endl;
			update_view_matrix();
			Globals::rotation_angle.data[1] =0;
		}
		
		if(Globals::key_left)
		{
			Globals::rotation_angle.data[1] = Globals::rotation_factor; // y
			//std::cout << "Globals::rotation_angle.data[1]: " << Globals::rotation_angle.data[1] << std::endl;			
			update_view_matrix();
			Globals::rotation_angle.data[1] =0		;	
		
		}







		// Clear screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Send updated info to the GPU
		glUniformMatrix4fv( shader.uniform("model"), 1, GL_FALSE, Globals::model.m  ); // model transformation
		glUniformMatrix4fv( shader.uniform("view"), 1, GL_FALSE, Globals::view.m  ); // viewing transformation
		glUniformMatrix4fv( shader.uniform("projection"), 1, GL_FALSE, Globals::projection.m ); // projection matrix
		glUniform3f( shader.uniform("eye"), Globals::eye[0], Globals::eye[1], Globals::eye[2] ); // used in fragment shader

		// Draw
		glDrawElements(GL_TRIANGLES, Globals::mesh.faces.size()*3, GL_UNSIGNED_INT, 0);

		// Finalize
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // end game loop

	// Unbind
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Disable the shader, we're done using it
	shader.disable();
    
	return EXIT_SUCCESS;
}


void init_scene(){

	using namespace Globals;

	
	
	
	// Adjust initial projection matrix values
	update_projection_matrix();

	
	// Adjust initial view matrix values
	Globals::rotation_angle.data[1] = 89.5; // y
	update_view_matrix();
	Globals::rotation_angle.data[1] =0;	


	
	
	// Create the buffer for vertices
	glGenBuffers(1, verts_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, verts_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, mesh.vertices.size()*sizeof(mesh.vertices[0]), &mesh.vertices[0][0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Create the buffer for colors
	glGenBuffers(1, colors_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, colors_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, mesh.colors.size()*sizeof(mesh.colors[0]), &mesh.colors[0][0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Create the buffer for normals
	glGenBuffers(1, normals_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, normals_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, mesh.normals.size()*sizeof(mesh.normals[0]), &mesh.normals[0][0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Create the buffer for indices
	glGenBuffers(1, faces_ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faces_ibo[0]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.faces.size()*sizeof(mesh.faces[0]), &mesh.faces[0][0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Create the VAO
	glGenVertexArrays(1, &tris_vao);
	glBindVertexArray(tris_vao);

	int vert_dim = 3;

	// location=0 is the vertex
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, verts_vbo[0]);
	glVertexAttribPointer(0, vert_dim, GL_FLOAT, GL_FALSE, sizeof(mesh.vertices[0]), 0);

	// location=1 is the color
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colors_vbo[0]);
	glVertexAttribPointer(1, vert_dim, GL_FLOAT, GL_FALSE, sizeof(mesh.colors[0]), 0);

	// location=2 is the normal
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normals_vbo[0]);
	glVertexAttribPointer(2, vert_dim, GL_FLOAT, GL_FALSE, sizeof(mesh.normals[0]), 0);

	// Done setting data for the vao
	glBindVertexArray(0);

}

