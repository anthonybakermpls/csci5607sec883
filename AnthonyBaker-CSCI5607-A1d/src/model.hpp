#ifndef MODEL_HPP
#define MODEL_HPP




#include "entity.hpp"
#include "entities.hpp"









class Material
{
	public:
	Material();
	void print(){};
	
	vec3 Od; // diffuse
	vec3 Os; // color of specular highlight
	float ka=0;		// ambient coefficient
	float kd=0;		// diffuse coefficient
	float ks=0;		// specular coefficient
	int n=0;		// Phong exponent - controls apparent shininess
	float alpha=1; // opacity
	float eta=1;   // index of refraction
	Material& operator = (const Material& other);
};
Material::Material(){}

/** define deep copy */
Material& Material::operator = (const Material& other)
{
	if (this != &other) // protect against invalid self-assignment
	{
		this->Od = other.Od;
		this->Os = other.Os;
		this->ka = other.ka;
		this->kd = other.kd;		
		this->ks = other.ks;
		this->n = other.n;
		this->alpha = other.alpha;
		this->eta = other.eta;		
	}

	return *this;
}




class PPM
{
	public:
	std::string magic_number="P3";
	std::string header_comment="#";
	int w=0;
	int h=0;
	int max_color=255;

	std::vector<int> data;
};


/* not used
class Texture
{
	public:
	Texture();
	void print();
	Texture& operator = (const Texture&);

	
	vec2 uv1;
	vec2 uv2;
	vec2 uv3;
	vec2 uv4;	

	std::string ppm_name;
	PPM ppm;


};
Texture::Texture(){}
void Texture::print()
{
	std:: cout << "Texture" << std::endl;
	std::cout << "PPM file: " << ppm_name << std::endl;
}



Texture& Texture::operator = (const Texture& other)
{
	this->uv1 = other.uv1;
	this->uv2 = other.uv2;
	this->uv3 = other.uv3;
	//this->uv4 = other.uv4;
	return *this;	
}

*/



/** Model is a another base class that all of our shapes will be derived from
 */
class Model : public Entity
{
	public:
	Model();
	virtual ~Model()=0;
	virtual void print()=0;
	virtual bool hit(const Ray&, float&)=0; // ray, something to hold t intersection point
	virtual vec3 get_normal(const vec3&) const = 0;
	virtual Bounding_box bounding_box()=0;
	virtual vec2 get_uv(const vec3&)=0;
	
	std::string name="Model";	
	Material material;
	bool is_textured=0;
	bool is_smooth=0;
	std::string texture_name=""; // maps to TExture ppm_name

};

Model::Model(){}
Model::~Model(){}





#endif
