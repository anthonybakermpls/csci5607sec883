#ifndef IO_HPP
#define IO_HPP





/**
 * class Input_file_parser
 * 
 * At some point we can make this super generic that can handle files 
 * based on a file extension or a spec or something.
 */




/// stdlib includes
#include <memory>
#include <string>
#include <vector>

/// project includes
#include "entity.hpp"
#include "model.hpp"
#include "parm_obj.hpp"
#include "scene.hpp"
#include "vec.hpp"
#include "util.hpp"




class IO
{
public:
	IO(){}
	void read_scene_file(const std::string& , Parm_obj&, Scene&);
	void read_ppm(const std::string& filename, PPM& ppm);
	void write_ppm(const std::string&, const Parm_obj&, const Scene& scene);
};





void IO::read_scene_file(const std::string& filename, Parm_obj& parm_obj, Scene& scene)
{

	// Are there any input file name requirements? I guess we can at least check the extension.
	if (filename.size() < 5)
	{throw std::runtime_error ("Malformed file name");}

	std::string ext = filename.substr(filename.size() -4);

// 	if ( ext != ".txt") 
// 	{throw std::runtime_error ("Incorrect file extension.");}

	// Create a filestream for reading the text file, and open it.
	std::ifstream inputfile_ifs;

	/// TODO figure out this exception
	//inputfile_ifs.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
	//try{

	std::cout << "Reading scenefile: " + filename << std::endl;
	inputfile_ifs.open( filename.c_str() );
	if( !inputfile_ifs.is_open() )
	{throw std::runtime_error ("Could not open file." + filename);}


	// Read input file and try to handle bad cases
	std::string var="";
	std::string line="";
	
	// do one getline and if we see eof then the file is empty.
	std::getline( inputfile_ifs, line );
	if(inputfile_ifs.eof())
	{ 
		throw std::runtime_error ("File empty.");
	}
	else  // clear eof bit and rewind
	{
		inputfile_ifs.clear();
		inputfile_ifs.seekg(0, inputfile_ifs.beg);
	}

	bool got_eye=false;
	bool got_viewdir=false;
	bool got_updir=false;
	bool got_fovv=false;
	bool got_imsize=false;
	bool got_bkgcolor=false;
	

	


	// These are local copes that are here to be filled by the while loop and then inserted into a container when full

	Material material;// a local copy that will be used for every shape and only changed when a new mtlcolor line is found
	//Texture texture;// a local copy that will be used for every shape and only changed when a new line is found
	std::string texture_name;
	bool texture_flag = 0; // this indicates if we've loaded a texture already or not. IF so, set every object afterward to use it.
	vec3 state_v;
	Face state_f;


	int line_count=0;

	while( std::getline( inputfile_ifs, line ) )
	{
		line_count++;
		
		var="";
		std::stringstream ss;
		
		if(line.length() != 0) // allow blank lines
		{
			ss << line;
			ss >> var;
		}
		
		// We can't use switch on string types so it has to be an if else chain

		if( var == "")
		{
			;
		}
		
		else if( var == "#")
		{
			;
		}
		
		else if( var == "eye" )
		{
			got_eye=true;
			ss >> parm_obj.eye.x; 
			ss >> parm_obj.eye.y; 
			ss >> parm_obj.eye.z;
		}
		
		else if( var == "viewdir" )
		{
			got_viewdir=true;
			ss >> parm_obj.view_dir.x; 
			ss >> parm_obj.view_dir.y; 
			ss >> parm_obj.view_dir.z; 	
			if( fabs(norm(parm_obj.view_dir)) -1 > FLT_EPSILONe04) //if( std::fabs(length(parm_obj.view_dir) - 1) <= FLT_EPSILONe04 )
			{
				// don't throw just warn and normalize
				//throw std::runtime_error ("Input vector not unit length: viewdir.");}
				std::cerr << "Warning: input vector viewdir not unit length. Normalizing." << std::endl;
				parm_obj.view_dir= normalize( parm_obj.view_dir );
			}
		}

		else if( var == "updir" )
		{
			got_updir=true;
			ss >> parm_obj.up_dir.x; 
			ss >> parm_obj.up_dir.y; 
			ss >> parm_obj.up_dir.z; 	
			if (std::fabs(norm(parm_obj.up_dir) - 1) > FLT_EPSILONe04)
			{
				//std::cerr << "std::fabs(norm(parm_obj.up_dir) - 1) : " << std::fabs(norm(parm_obj.up_dir) - 1) << std::endl;
				// don't throw just warn and normalize
				//throw std::runtime_error ("Input vector not unit length: updir.");
				std::cerr << "Warning: input vector updir not unit length. Normalizing." << std::endl;
				parm_obj.up_dir= normalize( parm_obj.up_dir );
			}	
			
		}

		else if( var == "fovv" )
		{
			got_fovv=true;
			ss >> parm_obj.fovv; 
			if( (parm_obj.fovv <0) || (parm_obj.fovv >180) )
			{throw std::runtime_error ("fovv out of range.");}				
		}

		else if( var == "imsize" )
		{
			got_imsize=true;
			ss >> parm_obj.imsize_w >> parm_obj.imsize_h;
			if( (parm_obj.imsize_w <1) || (parm_obj.imsize_h <1) )
			{throw std::runtime_error ("image size out of range.");}				
		}

		else if( var == "bkgcolor" )
		{
			got_bkgcolor=true;
			float r;
			float g;
			float b;						
			ss >> r;ss >> g;ss >> b;
				
			if(
				(r <0) || (r >1) ||
 				(g <0) || (g >1) ||
				(b <0) || (b >1)
			)
			{throw std::runtime_error ("bkgcolor out of range.");}	

			// we only set this once so that is why we do push_back instead of at() like in the mat_color situation
			scene.bg_color.x= r; 
			scene.bg_color.y=g;
			scene.bg_color.z=b;				
		}

		
		// This keyword lines can apppear any number of times and sets the color for objects after until
		// another mtlcolor is called.
		else if( var == "mtlcolor" ) // only change the local copy
		{
			texture_flag=0;
			
			// TODO: make sure all are read in properly
			ss >> material.Od.x;
			ss >> material.Od.y;
			ss >> material.Od.z;
			ss >> material.Os.x;
			ss >> material.Os.y;
			ss >> material.Os.z;
			ss >> material.ka ;
			ss >> material.kd ;
			ss >> material.ks ;
			ss >> material.n  ;
			ss >> material.alpha;
			ss >> material.eta;

			// range checking
			if(
				(material.Od.x <0 || material.Od.x >1) || 
				(material.Od.y <0 || material.Od.y >1) || 
				(material.Od.z <0 || material.Od.z >1) || 
				(material.Os.x <0 || material.Os.x >1) || 
				(material.Os.y <0 || material.Os.y >1) || 
				(material.Os.z <0 || material.Os.z >1) || 
				(material.ka  <0 || material.ka >1) ||
				(material.kd  <0 || material.kd >1) ||
				(material.ks  <0 || material.ks >1) || 
				(material.n   <0 || material.n >1000) ||
				(material.alpha   <0 || material.alpha >1) ||
				(material.eta   <0 || material.eta > 3000)
			)
			{throw std::runtime_error ("mtlcolor missing or out of range. Found on line " + std::to_string(line_count) + " of input");}	
		}

		
		// These keyword lines can apppear any number of times
		else if( var == "sphere" )
		{
			std::unique_ptr<Sphere> s_uptr(new Sphere);
			ss >> s_uptr->origin.x;
			ss >> s_uptr->origin.y;
			ss >> s_uptr->origin.z;
			ss >> s_uptr->r;
			s_uptr->name = "sphere";
			s_uptr->material = material;
			if(texture_flag)
			{
				s_uptr->is_textured=1;
				s_uptr->texture_name = texture_name;
			}
			scene.model_uptr_v.push_back(std::move(s_uptr));
		}

		/*
		else if( var == "ellipsoid" )
		{
			Ellipsoid e;
			ss >> e.x >> e.y >> e.z >> e.rx >> e.ry >> e.rz;
			e.material = material; // set to the current material
			e.texture_name = texture.ppm_name; // set to current texture file			
			scene.ellipsoid_v.push_back(e);
		}
		*/
		
		// The triangle input block.
		else if( var == "v" )
		{
			vec3 tr;
			ss >> tr.x; ss >> tr.y; ss >> tr.z; // pull in the v
			scene.vertices_v.push_back(tr);
		}

		// The normal
		else if( var == "vn" )
		{
			vec3 vn;
			ss >> vn.x; ss >> vn.y; ss >> vn.z; // pull in
			scene.normals_v.push_back(vn);
		}

		// The normal
		else if( var == "vt" )
		{
			vec2 tc;
			ss >> tc.x; ss >> tc.y;
			scene.tex_coords_v.push_back(tc);
		}



		else if( var == "f" ) // face specifications
		{

			Face f;

			// these are now complicated and requre a sub-parsing
			// look for /'s and if we encounter them, drop into special logic

			
			std::string token("/");
			std::vector<size_t> positions; // holds all the positions that sub occurs within string

			std::string ss_string(ss.str());

			size_t pos = ss_string.find(token, 0);
			while(pos != std::string::npos)
			{
				positions.push_back(pos);
				pos = ss_string.find(token,pos+1);
			}


			// \attention Attention! The assignment instructions state that the input file shall use face indices starting at 1
			// so that is why we are subtracting one every time we store.

			
			if(positions.size() == 0) // not textured not smooth shaded
			{
				// syntax: f v1
				ss >> f.v1;
				ss >> f.v2;
				ss >> f.v3;

				f.v1 -= 1; // setting to 0-n-1
				f.v2 -= 1; 
				f.v3 -= 1;
			}
			
			else if(positions.size() == 3) // textured not smooth shaded
			{
				f.is_textured=1;
				
				// syntax: f v1/vt1 v2/vt2 v3/vt3

				std::string temps;
				std::vector<std::string> pieces; // holds the coordinate peices of v1/vt1 etc;				
				std::vector<int> elements; // indices therefore int data type

				std::string data = ss.str().substr(2); // copy entire ss contents minus the v or f indicator

				// first parse on space
				std::istringstream is(data); 
				while(std::getline(is, temps, ' ')) {
					pieces.push_back(temps); // this now holds (v1/vt1,v2/vt2,v3/vt3)
				}

				for(auto x: pieces)
				{
					std::istringstream is2(x); 
					while(std::getline(is2, temps, '/')) {
						elements.push_back(std::stoi( temps )); // this will hold (v1,vt1,v2,vt2,v3,vt3)
					}
				}
				f.v1 = (elements.at(0) -1 );
				f.v2 = (elements.at(2) -1 );
				f.v3 = (elements.at(4) -1 );

				f.uv0_idx = (elements.at(1) -1 );
				f.uv1_idx = (elements.at(3) -1 );
				f.uv2_idx = (elements.at(5) -1 );
								
			}
			else if(positions.size() == 6) // textured and smooth shaded
			{
				// syntax: f v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3

				f.is_textured=1;
				f.is_smooth=1;

				std::string temps;
				std::vector<std::string> pieces; // holds the coordinate peices of v1/vt1 etc;				
				std::vector<int> elements; // indices therefore int data type

				std::string data = ss.str().substr(2); // copy entire ss contents minus the v or f indicator

				// first parse on space
				std::istringstream is(data); 
				while(std::getline(is, temps, ' ')) {
					pieces.push_back(temps); // this now holds (v1/vt1/vn1,v2/vt2,vn2,v3/vt3/vn3)
				}

				for(auto x: pieces)
				{
					std::istringstream is2(x);
					while(std::getline(is2, temps, '/'))
					{
						elements.push_back(std::stoi( temps )); // this will hold (v1,vt1,vn1,v2,vt2,vn3,v3,vt3,vn3)
					}
				}


				f.v1 = (elements.at(0) -1);
				f.v2 = (elements.at(3) -1);
				f.v3 = (elements.at(6) -1);
                    
				f.n0_idx = (elements.at(1) -1);
				f.n1_idx = (elements.at(4) -1);
				f.n2_idx = (elements.at(7) -1);

				f.uv0_idx = (elements.at(2) -1);
				f.uv1_idx = (elements.at(5) -1);
				f.uv2_idx = (elements.at(8) -1);				
				
			}

			
			//scene.face_vertices_v.push_back(f.v1);  // may ditch this
			//scene.face_vertices_v.push_back(f.v2);   // may ditch this			
			//scene.face_vertices_v.push_back(f.v3);   // may ditch this

			std::unique_ptr<Tri> t_uptr = create_triangle_uptr(scene, f);
			t_uptr->name = "triangle";
			t_uptr->material = material;			
			if(texture_flag)
			{
				t_uptr->texture_name = texture_name;
				t_uptr->is_textured = 1;				
			}

			scene.model_uptr_v.push_back(std::move(t_uptr));	
		}

		//s.material = material; // set to the current material



		else if( var == "light" )
		{
			Light light;

			ss >> light.origin.x;
			ss >> light.origin.y;
			ss >> light.origin.z;
			ss >> light.w;

			ss >> light.intensity.x;
			ss >> light.intensity.y;
			ss >> light.intensity.z;

			clamp(light.intensity, 0.f, 1.f);
			scene.light_v.push_back(light); // store permanently
		}



		// This keyword lines can apppear any number of times and sets the color for objects after until
		// another is called.
		else if( var == "texture" ) // only change the local copy
		{

			ss >> texture_name;

			texture_flag = 1;

			// check if this texture has already been loaded. If not, store it.
			auto it = scene.textures_stdmap.find(texture_name);
			if( it==scene.textures_stdmap.end() )
			{
				PPM ppm;
				read_ppm(scene.texture_dir + texture_name,ppm);
				scene.textures_stdmap[texture_name]=ppm;
			}
			
		}




		
		/*
		else if( var == "spotlight" )
		{
			Spotlight spotlight;

			ss >> spotlight.origin.x;
			ss >> spotlight.origin.y;
			ss >> spotlight.origin.z;
			ss >> spotlight.w;

			ss >> spotlight.color.x;
			ss >> spotlight.color.y;
			ss >> spotlight.color.z;

			clamp(spotlight.color, 0.f, 1.f);

			scene.light_v.push_back(spotlight); // store permanently	
		}
		*/
		else
		{}
		

	}// end while getline loop

	
	if (inputfile_ifs.bad()) /// TODO figure out how to use exceptions for this ios_base
	{ 
		std::cerr << "**Error: Read/writing error on i/o operation. " << filename << std::endl; 
		exit(1); 
	}

		



	/** Do any checks that require comparison between input values or that require more than one value to compare or any general limits.
	*/


	// first check that we actually got all required parameters
	if(
	(got_eye==false)		||
	(got_viewdir==false)	|| 
	(got_updir==false)		||
	(got_fovv==false)		||
	(got_imsize==false)		||
	(got_bkgcolor==false)
	)
	{
		// I think throwing is appropriate here
		throw std::runtime_error ("Missing input.");
		//std::cerr << "Warning: Missing input. Using defaults" << std::endl;
	}









	// view and up cannot be parallel
	vec3 temp= cross(parm_obj.view_dir,parm_obj.up_dir);
	
//	std::cout << "up_dir: " << parm_obj.up_dir.to_str() << std::endl;
//	std::cout << "view_dir: " << parm_obj.view_dir.to_str() << std::endl;	
//	std::cout << "view_dir cross up_dir: " << temp.to_str() << std::endl;
	if( (std::fabs(temp.x ) <= FLT_EPSILONe04) &&
	    (std::fabs(temp.y ) <= FLT_EPSILONe04) &&
	    (std::fabs(temp.z ) <= FLT_EPSILONe04) 
	)
	{throw std::runtime_error ("Input view and up vectors are parallel.");}	




	// imsize constraints
	if( 
		   (parm_obj.imsize_w == 0)
		|| (parm_obj.imsize_h == 0)
		|| (parm_obj.imsize_w < 0) 
		|| (parm_obj.imsize_h < 0)
		|| (parm_obj.imsize_w > 4000) // 4k enough? 
		|| (parm_obj.imsize_h > 4000)
	)
	{throw std::runtime_error ("Failed image dimensions.");}	





}







/**
 *  All of the header information is provided on a single line and comments are not present.
 *
 *
 */
void IO::read_ppm(const std::string& filename, PPM& ppm)
{


	
	// Are there any input file name requirements? I guess we can at least check the extension.
	if (filename.size() < 5)
	{throw std::runtime_error ("Malformed file name");}

	std::string ext = filename.substr(filename.size() -4);

	if ( ext != ".ppm") 
	{throw std::runtime_error ("Incorrect file extension. Got " + ext);}

	// Create a filestream for reading the text file, and open it.
	std::ifstream inputfile_ifs;

	/// TODO figure out this exception
	//inputfile_ifs.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
	//try{

	inputfile_ifs.open( filename.c_str() );
	if( !inputfile_ifs.is_open() )
	{throw std::runtime_error ("Could not open file: " + filename);}


	// Read input file and try to handle bad cases
	std::string var="";
	std::string line="";
	
	// do one getline and if we see eof then the file is empty.
	std::getline( inputfile_ifs, line );
	if(inputfile_ifs.eof())
	{ 
		throw std::runtime_error ("File empty.");
	}
	else  // clear eof bit and rewind
	{
		inputfile_ifs.clear();
		inputfile_ifs.seekg(0, inputfile_ifs.beg);
	}


	std::stringstream ss;
	// read header
	std::getline( inputfile_ifs, line );
	ss << line;
	ss >> ppm.magic_number;
	ppm.header_comment = "# " + filename;
	ss >> ppm.w;
	ss >> ppm.h;
	ss >> ppm.max_color;

	// handle when all values are on own line + when rgb is all on one line
	
	int value=0;
	while( std::getline( inputfile_ifs, line ) )
	{
		std::stringstream ss2;
		ss2 << line;

		
		// test 
		while( ss2 )
		{
			ss2 >> value;
			if(ss2)
				ppm.data.push_back(value);
		}
		

	}



}











/**
 * Write the ouptut image file
 */
void IO::write_ppm(const std::string& filename, const Parm_obj& parm_obj, const Scene& scene)
{
	// build a PPM header
	// Format of our input file is keyword "imgsize" and then two integers specifiying the dimension
	// Format of output is a magic number, comment, dimensions, color range, and then our pixel values in RGB tuples.	
	std::string magic_number = "P3";

	std::string ppm_header(
	magic_number+"\n"
	+parm_obj.comment+"\n"
	+std::to_string(parm_obj.imsize_w)+" "+std::to_string(parm_obj.imsize_h)+"\n"
	+std::to_string(scene.max_color)+"\n"
	);

	// Create a filestream for outputting the text file, and open it. Quit program if there's an error
	std::ofstream outfile( filename.c_str() );
	if( !outfile.is_open() )
	{ std::cerr << "**Error: Could not open output file " << filename << std::endl; exit(1); }
	
	// Write the header
	outfile << ppm_header; // last item already has a newline


	// Write the pixel RGB values
	std::cout << "Writing " << filename << '\n';
	int line_length=0; // allow us to write more than one value per line
	for(auto i : parm_obj.pixel_values)
	{
		outfile << i;
		line_length++;
		if(line_length >14)    // 3 digts + 1 space * 3 components = 12 bytes max size of one pixel (5 pixels * 12 bytes = 60)
		{
			line_length=0;
			outfile << '\n';
		}
		else{
			outfile << " ";
		}
	}
}


#endif
