#ifndef UTIL_HPP
#define UTIL_HPP

#include <cmath>
#include "float.h" // FLT_EPSILON
#include "vec.hpp"


/** Misc. constants, functions whatever
 */



#define PI 3.14159265f
//*PI/180.0



/*
std::ostringstream s1;
s1 << FLT_EPSILON;
std::string s(s1.str());
std::cout << "FLT_EPSILON: " << s << std::endl;
0000000119209

std::ostringstream s1;
s1 << FLT_EPSILONe04;
std::string s(s1.str());
std::cout << "FLT_EPSILONe04: " << s << std::endl;

*/

#define FLT_EPSILONe03 FLT_EPSILON*1000

#define FLT_EPSILONe04 FLT_EPSILON*10000



int denormalize_color(const float& f, const int& max_denorm_color )
{
	return floor(  std::fabs(f-1) <= FLT_EPSILON  ? max_denorm_color : (f * (max_denorm_color + 1)) );
}

float normalize_color(const int& in_color, const int& max_denorm_color )
{
	// map 0 - max_denorm_color   to 0-1
	return (float)in_color/(float)max_denorm_color;
}



/** Clamp a float to an interval */
float clamp(const float& f, const float& min, const float& max)
{
	if(f < min)
		return min;
	else if (f > max)
		return max;

	return f;
}


/** Clamp a vec3 to a float interval */
vec3 clamp(const vec3& f, const float& min, const float& max)
{
	vec3 temp =f;
	
	if(temp.x < min)
		temp.x = min;
	else if (temp.x > max)
		temp.x = max;

	if(temp.y < min)
		temp.y = min;
	else if (temp.y > max)
		temp.y = max;

	if(temp.z < min)
		temp.z = min;
	else if (temp.z > max)
		temp.z = max;

	return temp;
}





bool zero_float_equal(const float& f1, const float& f2)
{
	if(std::fabs(f1-f2) <= FLT_EPSILON)
		return true;
	return false;
}











// see https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
bool nonzero_float_equal_relative(float A, float B, float maxRelDiff = FLT_EPSILON)
{
    // Calculate the difference.
    float diff = std::fabs(A - B);
    A = std::fabs(A);
    B = std::fabs(B);
    // Find the largest
    float largest = (B > A) ? B : A;
 
    if (diff <= largest * maxRelDiff)
        return true;
    return false;
}









#endif
