#ifndef ENTITY_HPP
#define ENTITY_HPP


/**
 * Everything in the scene will be an entity
 */

#include "vec.hpp"







class Entity
{

	public:
	Entity(){};
	virtual ~Entity()=0;
	virtual void print()=0;
	
	vec3 origin;
	
	//virtrual box bounding-box();
};
Entity::~Entity(){}






	

#endif
