﻿Anthony Baker
CSCI 5607 SEC 883

Assignment 1d:
This program demonstrates the use of reflection and transparency




To build:

mkdir build;
cd build;
cmake ..;
make;



Note! The textures are assumed to be in the same dir as the executable per TA instruction. So for example in this assignment do this before running:
	cp assign1d_demo.txt build/assign1d_demo.txt;
	cp mandelbrot.ppm build/mandelbrot.ppm;



Usage:
./assignment1d input.txt


