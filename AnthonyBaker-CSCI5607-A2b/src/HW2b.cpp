// Skeleton code for hw2b
// Based on example code from: Interactive Computer Graphics: A Top-Down Approach with Shader-Based OpenGL (6th Edition), by Ed Angel


#ifdef USE_GLEW
    #include <GL/glew.h>
#else
    #define GLFW_INCLUDE_GLCOREARB
#endif


#include <cassert>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <sstream>
#include <vector>

// This file contains the code that reads the shaders from their files and compiles them
#include "ShaderStuff.hpp"
#define DEBUG 1



/*
a 0 0 0
b 0 0 0
c 0 0 0
d 0 0 0

M[a,b,c,d,0,0,0,0,...]

index for row,column (r,c) =  ((c-1)*4 + (r-1)
(3,2) = M[1*4 + 3-1]  = M[6]

*/

int index(const int& r, const int& c)
{
	return ((c-1)*4) + (r-1);
}



GLdouble PI = 4.0*atan(1.0);



GLfloat M[16]; // general transformation matrix
GLfloat I[16]; // identity matrix

GLfloat translate_m[16]; // translation
GLfloat rotate_m[16]; // rotation
GLfloat scale_m[16]; // scale

GLint m_location; // location of uniform for M in shader

GLdouble mouse_x, mouse_y;
GLdouble prev_mouse_x, prev_mouse_y;
GLdouble mouse_dx, mouse_dy;

bool alt_key; // save this state accross callbacks
bool mouse_button_left;
bool lmb_pressed;



GLint window_width = 600;
GLint window_height = 600;


GLFWcursor *hand_cursor, *arrow_cursor;



// initialize some basic structures and geometry
typedef struct {
    GLfloat x, y;
} FloatType2D;

typedef struct {
    GLfloat r, g, b;
} ColorType3D;

typedef struct {
    GLfloat x, y, z, w;
} FloatType4D;



const int nvertices = 4;






FloatType2D object_location;
FloatType2D object_scale;
float theta=0;
int axis[3];

// scaling factors (check relationship to pixel )
GLfloat scale_factor=0.05;
GLfloat translation_factor=0.003;
GLfloat rotation_factor=1;







void print_m(const float m[16])
{
	printf("M = [%f %f %f %f\n     %f %f %f %f\n     %f %f %f %f\n     %f %f %f %f]\n",
		  m[0],m[4],m[8],m[12], 
		  m[1],m[5],m[9],m[13], 
		  m[2],m[6],m[10],m[14], 
		  m[3],m[7],m[11],m[15]
	);
}








// multiply two 4x4 matrices together and store result in result
// only the result is passed by reference because more than likely ls or rs will be the same matrix as result and we don't want to clobber each other
// validate this understanding.
void mat4_mult(GLfloat cpyA[16], GLfloat cpyB[16], GLfloat result[16])
{

	GLfloat A[16];
	GLfloat B[16];
	
    for (int i=0; i<16; i++) {
         A[i] = cpyA[i];
    }
    for (int i=0; i<16; i++) {
         B[i] = cpyB[i];
    }
    
	result[0]=  (A[0]* B[0]) + (A[4]* B[1]) +  (A[8]*  B[2]) +  (A[12]*  B[3]);
	result[1]=  (A[1]* B[0]) + (A[5]* B[1]) +  (A[9]*  B[2]) +  (A[13]*  B[3]);
	result[2]=  (A[2]* B[0]) + (A[6]* B[1]) +  (A[10]* B[2]) +  (A[14]*  B[3]);
	result[3]=  (A[3]* B[0]) + (A[7]* B[1]) +  (A[11]* B[2]) +  (A[15]*  B[3]);
                                                             
	result[4]=  (A[0]* B[4]) + (A[4]* B[5]) +  (A[8]*  B[6]) +  (A[12]*  B[7]);
	result[5]=  (A[1]* B[4]) + (A[5]* B[5]) +  (A[9]*  B[6]) +  (A[13]*  B[7]);
	result[6]=  (A[2]* B[4]) + (A[6]* B[5]) +  (A[10]* B[6]) +  (A[14]*  B[7]);
	result[7]=  (A[3]* B[4]) + (A[7]* B[5]) +  (A[11]* B[6]) +  (A[15]*  B[7]);
                                            
	result[8]=  (A[0]* B[8]) + (A[4]* B[9]) +  (A[8]*  B[10]) + (A[12]*  B[11]);
	result[9]=  (A[1]* B[8]) + (A[5]* B[9]) +  (A[9]*  B[10]) + (A[13]*  B[11]);
	result[10]= (A[2]* B[8]) + (A[6]* B[9]) +  (A[10]* B[10]) + (A[14]*  B[11]);
	result[11]= (A[3]* B[8]) + (A[7]* B[9]) +  (A[11]* B[10]) + (A[15]*  B[11]);

	result[12]= (A[0]* B[12]) + (A[4]* B[13]) + (A[8]*  B[14]) + (A[12]* B[15]) ;
	result[13]= (A[1]* B[12]) + (A[5]* B[13]) + (A[9]*  B[14]) + (A[13]* B[15]) ;
	result[14]= (A[2]* B[12]) + (A[6]* B[13]) + (A[10]* B[14]) + (A[14]* B[15]) ;
	result[15]= (A[3]* B[12]) + (A[7]* B[13]) + (A[11]* B[14]) + (A[15]* B[15]) ;
	

}


void translate(const FloatType2D& p)
{
	// 2d version homogenous coordinates

	translate_m[12]=p.x;
	translate_m[13]=p.y;
	translate_m[14]=0; // z not used
	//translate_m[15]=1; // w	already set identity
}

void rotate(GLfloat m[16], const int axis[3], const float& theta)
{
	//a c 0 0
	//b d 0 0
	//0 0 1 0
	//0 0 0 1
	float theta_r = (theta*PI)/180; // convert degrees to radians
	
	if(axis[0]) // Rx(theta)
	{
		m[5]=cos(theta_r);
		m[9]=-sin(theta_r);
		m[6]=sin(theta_r);
		m[10]=cos(theta_r);
	}
	else if(axis[1]) // Ry(theta)
	{
		m[0]=cos(theta_r);
		m[8]=sin(theta_r);
		m[2]=-sin(theta_r);
		m[15]=cos(theta_r);
	}
	else if (axis[2]) // Rz(theta)
	{
		m[0]=cos(theta_r);
		m[4]=-sin(theta_r);
		m[1]=sin(theta_r);
		m[5]=cos(theta_r);
		//print_m(rotate_m);
	}
}

void scale(const FloatType2D& p)
{
	//scale_m[16];
	
	// 2d version homogenous coordinates
	scale_m[0]=p.x;
	scale_m[5]=p.y;
	scale_m[10]=1;
	//translate_m[15]=1; // w	already set identity	
}


void ident(float m[16])  // initialize an identity matrix
{
    for (int i=0; i<16; i++) {
        m[i] = (i%5==0);
    }
}

void reset()
{
	object_location.x=0;
	object_location.y=0;
	object_scale.x=1;
	object_scale.y=1;
	theta=0;		

	axis[0]=0;
	axis[1]=0;
	axis[2]=1;
	
    // initialize an identity matrix
	ident(I);
	
	ident(translate_m);
	ident(rotate_m);
	ident(scale_m);
}
















//----------------------------------------------------------------------------
// function that is called whenever an error occurs
static void
error_callback(int error, const char* description){
    fputs(description, stderr);  // write the error description to stderr
}



//----------------------------------------------------------------------------
// function that is called whenever a keyboard event occurs; defines how keyboard input will be handled
static void
key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    
    switch ( key ) 
	{
        case 256:  // ESC
        case 'q':
        case 'Q':
            glfwSetWindowShouldClose(window, GL_TRUE);
            break;
			
		case 'r':
		case 'R':
			reset();
			break;
		
		case 263: // left arrow
			if( object_scale.x-scale_factor > 0) // Don't dissapear or go negative
				object_scale.x-=scale_factor; // -scale on x 
			break;	
			
		case 262: // right arrow
			if( object_scale.x+scale_factor > 0) // Don't dissapear or go negative
				object_scale.x+=scale_factor; // -scale on x 
			break;	
    
		case 265: // up arrow
			object_scale.y+=scale_factor;// +scale on y
			break;	
			
		case 264: // down arrow
			if( object_scale.y-scale_factor > 0) // Don't dissapear or go negative
				object_scale.y-=scale_factor;// -scale on y	
			break;
		
		case 342: // left alt
			if(action = GLFW_PRESS)
				alt_key=true;
			else if(action = GLFW_RELEASE)
				alt_key=false;
			break;
	}
}

//----------------------------------------------------------------------------
// function that is called whenever a mouse or trackpad button press event occurs
static void
mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    glfwGetCursorPos(window, &mouse_x, &mouse_y);
    
    // Check which mouse button triggered the event, e.g. GLFW_MOUSE_BUTTON_LEFT, etc.
    // and what the button action was, e.g. GLFW_PRESS, GLFW_RELEASE, etc.
    // (Note that ordinary trackpad click = mouse left button)
    // Also check if any modifier keys were active at the time of the button press, e.g. GLFW_MOD_ALT, etc.
    // Take the appropriate action, which could (optionally) also include changing the cursor's appearance

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && mods == GLFW_MOD_ALT )
	{
		lmb_pressed=true;
		alt_key=true;
	}

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS && mods != GLFW_MOD_ALT)
	{
		lmb_pressed=true;
		alt_key=false;
	}

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE )
	{
		lmb_pressed=false;
		alt_key=false; // undo for either case
	}
}





//----------------------------------------------------------------------------
// function that is called whenever a cursor motion event occurs
static void
cursor_pos_callback(GLFWwindow* window, double xpos, double ypos) {
    
    // determine the direction of the mouse or cursor motion
    // update the current mouse or cursor location
    //  (necessary to quantify the amount and direction of cursor motion)
    // take the appropriate action


	mouse_dx= xpos - prev_mouse_x;
	mouse_dy= ypos - prev_mouse_y;

	if(lmb_pressed && !alt_key)
	{
		//clockwise when moust moved to the right
		// counterclockwise when mouse moved to the left
		if(mouse_dx < 0)
			theta+=(abs(mouse_dx)*rotation_factor);// move 

		if(mouse_dx > 0)
			theta-=(abs(mouse_dx)*rotation_factor);// move 	
		//std::cout << "mouse: (" << mouse_dx << "," << mouse_dy << ")" << std::endl;
	}
	if(lmb_pressed && alt_key)
	{
		// AFFECTS X COORDINATE
		if(mouse_dx < 0)
		{
			if( object_location.x-(abs(mouse_dx)*translation_factor) > -1) // check if in window
			{
				object_location.x-=(abs(mouse_dx)*translation_factor);// move 
			}
		}
		if(mouse_dx > 0)
		{
			if(object_location.x+(abs(mouse_dx)*translation_factor) < 1) // check if in window
				object_location.x+=(abs(mouse_dx)*translation_factor);// move 
		}

		// AFFECTS Y COORDINATE
		if(mouse_dy < 0)
		{
			if(object_location.y+(abs(mouse_dy)*translation_factor) < 1) // check if in window
				object_location.y+=(abs(mouse_dy)*translation_factor);// move 
		}	
		if(mouse_dy > 0)
		{
			if(object_location.y-(abs(mouse_dy)*translation_factor) > -1) // check if in window
				object_location.y-=(abs(mouse_dy)*translation_factor);// move 			
		}
	}



	prev_mouse_x = xpos;
	prev_mouse_y = ypos;
}













void init( void )
{
    int i;
    ColorType3D colors[4];
    FloatType2D vertices[4];
    GLuint vao[1], buffer, program, location1, location2;

	reset();




    // set up 4 base colors and 4 corners of a square
    colors[0].r = 1;  colors[0].g = 0;  colors[0].b = 0;  // red
    colors[1].r = 1;  colors[1].g = 1;  colors[1].b = 0;  // yellow
    colors[2].r = 0;  colors[2].g = 1;  colors[2].b = 0;  // green
    colors[3].r = 0;  colors[3].g = 0;  colors[3].b = 1;  // blue

    vertices[0].x = -0.5;  vertices[0].y = -0.5; // lower left
    vertices[1].x =  0.5;  vertices[1].y = -0.5; // lower right
    vertices[2].x =  0.5;  vertices[2].y =  0.5; // upper right
    vertices[3].x = -0.5;  vertices[3].y =  0.5; // upper left


	//A 2D coordinate passed to a vec4 attribute becomes vec4 (x, y, 0.0, 1.0)    
    
    
    // Create a vertex array object
    glGenVertexArrays( 1, vao );
    glBindVertexArray( vao[0] );

    // Create and initialize a buffer object large enough to hold both vertex position and color data
    glGenBuffers( 1, &buffer );
    glBindBuffer( GL_ARRAY_BUFFER, buffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof(vertices)+sizeof(colors), vertices, GL_STATIC_DRAW );
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(colors), colors);

    // Define the names of the shader files
    std::stringstream vshader, fshader;
    vshader << SRC_DIR << "/vshader2b.glsl";
    fshader << SRC_DIR << "/fshader2b.glsl";

    // Load the shaders and use the resulting shader program
    program = InitShader( vshader.str().c_str(), fshader.str().c_str() );

    // Determine locations of the necessary attributes and matrices used in the vertex shader
    location1 = glGetAttribLocation( program, "vertex_position" );
    glEnableVertexAttribArray( location1 );
    glVertexAttribPointer( location1, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
	
    location2 = glGetAttribLocation( program, "vertex_color" );
    glEnableVertexAttribArray( location2 );
    glVertexAttribPointer( location2, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(vertices)) );
    m_location = glGetUniformLocation( program, "M");

    // Define static OpenGL state variables
    glClearColor( 1.0, 1.0, 1.0, 1.0 ); // white, opaque background

    // Define some GLFW cursors (in case you want to dynamically change the cursor's appearance)
    // If you want, you can add more cursors, and even define your own cursor appearance
    arrow_cursor = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
    hand_cursor = glfwCreateStandardCursor(GLFW_HAND_CURSOR);

}

















int main(int argc, char** argv) {

    int i;
    GLFWwindow* window;

    // Define the error callback function
    glfwSetErrorCallback(error_callback);
    
    // Initialize GLFW (performs platform-specific initialization)
    if (!glfwInit()) exit(EXIT_FAILURE);
    
    // Ask for OpenGL 3.2
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    // Use GLFW to open a window within which to display your graphics
	window = glfwCreateWindow(window_width, window_height, "HW2b", NULL, NULL);
	
    // Verify that the window was successfully created; if not, print error message and terminate
    if (!window)
	{
        printf("GLFW failed to create window; terminating\n");
        glfwTerminate();
        exit(EXIT_FAILURE);
	}
    
	glfwMakeContextCurrent(window); // makes the newly-created context current
    
    // If not using a Mac, initialize GLEW
    #ifdef USE_GLEW
        glewExperimental = true;
        GLenum err = glewInit();
        // check for errors in GLEW initialization
        if (err != GLEW_OK) {
            cout << "error initializing GLEW: " << glewGetErrorString(err) << endl;
            exit(EXIT_FAILURE);
        }
    #endif
    
	glfwSwapInterval(1);  // tells the system to wait to swap buffers until monitor refresh has completed; necessary to avoid tearing

	// Define the keyboard callback function
    glfwSetKeyCallback(window, key_callback);
    // Define the mouse button callback function
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    // Define the mouse motion callback function
    glfwSetCursorPosCallback(window, cursor_pos_callback);

	
	
	// Required calls
	glewExperimental = GL_TRUE; 
	glewInit();



	// Create the shaders
	init();

	while (!glfwWindowShouldClose(window)) 
	{
		ident(M); // reset M to identity


		// build the matrices based on our parameters
		translate(object_location);
		rotate(rotate_m, axis, theta);
		scale(object_scale);

		// Multiply matrices: scale, rotate, translate
		mat4_mult(scale_m, M, M);
		mat4_mult(rotate_m, M, M);		
		mat4_mult(translate_m, M, M);

		
        // sanity check that your matrix contents are what you expect them to be
        //if (DEBUG) printf("M = [%f %f %f %f\n     %f %f %f %f\n     %f %f %f %f\n     %f %f %f %f]\n",M[0],M[4],M[8],M[12], M[1],M[5],M[9],M[13], M[2],M[6],M[10],M[14], M[3],M[7],M[11],M[15]);

		glClear( GL_COLOR_BUFFER_BIT );// fill the window with the background color		
		
        glUniformMatrix4fv( m_location, 1, GL_FALSE, M );   // send the model transformation matrix to the GPU
		glDrawArrays( GL_TRIANGLE_FAN, 0, nvertices );    // draw a triangle between the first vertex and each successive vertex pair
		glFlush();	// ensure that all OpenGL calls have executed before swapping buffers

        glfwSwapBuffers(window);  // swap buffers

		glfwWaitEvents(); // wait for an event, then handle it
	}

	// Clean up
	glfwDestroyWindow(window);
	glfwTerminate();  // destroys any remaining objects, frees resources allocated by GLFW
	exit(EXIT_SUCCESS);

} // end main


