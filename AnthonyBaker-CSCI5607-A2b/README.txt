﻿Anthony Baker
CSCI 5607 SEC 883

Assignment 2b : Programming Interactive 2D Graphics with OpenGL and GLFW


To build:

mkdir build;
cd build;
cmake ..;
make;


Usage: Binary in bin dir where the makefile was generated:

./${PATH TO THIS ASSIGNMENT}/build/bin/HW2b_App


