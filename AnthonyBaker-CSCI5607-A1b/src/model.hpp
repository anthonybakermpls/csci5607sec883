#ifndef MODEL_HPP
#define MODEL_HPP




#include "entity.hpp"
#include "entities.hpp"




/**
 * This file will hold the Model class which is a base class that all of our shapes will be derived from.
 * We also define the Material object here.
 */





class Material
{
	public:
	Material();
	void print(){};
	
	//float Odr=1;	// diffuse red
	//float Odg=0;	// diffuse green
	//float Odb=0;	// diffuse blue
	//float Osr=0;	// 
	//float Osg=0;	// 
	//float Osb=0;	// 

	vec3 Od; // diffuse
	vec3 Os; // color of specular highlight
	
	float ka=0;		// ambient coefficient
	float kd=0;		// diffuse coefficient
	float ks=0;		// specular coefficient
	int n=0;		// Phong exponent - controls apparent shininess

	Material& operator = (const Material& other);
};
Material::Material(){}

Material& Material::operator = (const Material& other)
{
	if (this != &other) // protect against invalid self-assignment
	{
		this->Od = other.Od;
		this->Os = other.Os;
		this->ka = other.ka;
		this->kd = other.kd;		
		this->ks = other.ks;
		this->n = other.n;
	}

	return *this;
}


class Box
{
	public:
	Box(){};
};








/** Model is a another base class that all of our shapes will be derived from
 */
class Model : public Entity
{
	public:
	Model();
	virtual void print()=0;
	virtual bool hit(const Ray&, const float&, const float&,  float& t)=0; // ray, t0, t1, something to hold t intersection point
	virtual vec3 get_normal(const vec3&) const = 0;
	virtual Box bounding_box()=0;
	Material material;
};
Model::Model(){}





/** Not sure what the use of this is yet. It was in the book.
 
struct Hit_record
{
	float t;
};
*/





#endif
