#ifndef VEC_HPP
#define VEC_HPP






/**
 * class vec2 and vec3, vec4 for w component although this isn't completed.
 * 
 * Has basic routines and overloaded operators.
 * 
 */







#include <sstream>










class vec2
{
public:
	vec2() : x(0), y(0) {}
	vec2(const float& x_,const float& y_ ) : x(x_), y(y_) {}
	float x, y;
	vec2& operator = (const vec2& );
	bool operator == (const vec2& ) const ;		
	vec2 operator + (const vec2& ) const;	
	vec2 operator - (const vec2& ) const;	
	vec2 operator * (const float& ) const;	
	vec2 operator / (const float& ) const;
	vec2 std_vector_to_vec2(const std::vector<float>&);	
	std::string to_str();
};



class vec3 
{
public:
	vec3() : x(0), y(0), z(0) {}
	vec3(const float& x_,const float& y_, const float& z_ ) : x(x_), y(y_), z(z_) {}
	float x, y, z;
	vec3& operator = (const vec3& );
	bool operator == (const vec3& ) const;	
	vec3 operator + (const vec3& ) const;		
	vec3 operator - (const vec3& ) const;			
	vec3 operator * (const float& ) const;	
	vec3 operator / (const float& ) const;
	vec3 std_vector_to_vec3(const std::vector<float>&);
	std::string to_str();
};


/** This has a w component. I don't know how far to take this
 */
class vec4 
{
public:
	vec4() : x(0), y(0), z(0), w(0) {}
	vec4(const float& x_,const float& y_, const float& z_, const float& w_ ) : x(x_), y(y_), z(z_), w(w_) {}
	float x, y, z, w;
	vec4& operator = (const vec4& );
	bool operator == (const vec4& ) const;		
	vec4 operator + (const vec4& ) const;		
	vec4 operator - (const vec4& ) const;			
	vec4 operator * (const float& ) const;	
	vec4 operator / (const float& ) const;
	vec4 std_vector_to_vec3(const std::vector<float>&);
	std::string to_str();
};



/***********************************************************************
 * vec2
 **********************************************************************/

vec2& vec2::operator = (const vec2& other)
{
	if (this != &other) // protect against invalid self-assignment
	{
		this->x=other.x;
		this->y=other.y;
	}

	return *this;
}

bool vec2::operator == (const vec2& other) const
{
	if(
		(this->x == other.x) && 
		(this->y == other.y)
		//(this->z==other.z
	)
		return true;
	else
		return false;
}

vec2 vec2::operator + (const vec2& other) const
{
	vec2 temp;
	temp.x = this->x+other.x;
	temp.y = this->y+other.y;
	return temp;
}

vec2 vec2::operator - (const vec2& other) const
{
	vec2 temp;
	temp.x = this->x-other.x;
	temp.y = this->y-other.y;
	return temp;
}

vec2 vec2::operator * (const float& scalar) const
{
	vec2 temp;
	temp.x = this->x *scalar;
	temp.y = this->y *scalar;
	return temp;
}

vec2 vec2::operator / (const float& scalar) const
{
	vec2 temp;
	temp.x = this->x /scalar;
	temp.y = this->y /scalar;
	return temp;
}

    
std::string vec2::to_str()
{
	std::stringstream ss;
	ss << '(' << x << ',' << y << ')';
	return ss.str();
}

float dot(const vec2& u, const vec2& v)
{
	float dot = u.x*v.x + u.y*v.y; 
	return dot;
}

vec2 cross(const vec2& u, const vec2& v)
{
	vec2 w;
	w.x = u.x*v.y - v.x*u.y; // double check this
	return w;
}


float length(const vec2& u)
{
	return sqrt((u.x*u.x) + (u.y*u.y));
}







/***********************************************************************
 * vec3
 **********************************************************************/


vec3& vec3::operator = (const vec3& other)
{
	if (this != &other) // protect against invalid self-assignment
	{
		this->x=other.x;
		this->y=other.y;
		this->z=other.z;		
	}
	return *this;
}
    
bool vec3::operator == (const vec3& other) const
{
	if(
		(this->x == other.x) && 
		(this->y == other.y) &&
		(this->z==other.z)
	)
		return true;
	else
		return false;
}

vec3 vec3::operator + (const vec3& other) const
{
	vec3 temp;
	temp.x = this->x+other.x;
	temp.y = this->y+other.y;
	temp.z = this->z+other.z;	
	return temp;
}

vec3 vec3::operator - (const vec3& other) const
{
	vec3 temp;
	temp.x = this->x-other.x;
	temp.y = this->y-other.y;
	temp.z = this->z-other.z;	
	return temp;
}

vec3 vec3::operator * (const float& scalar) const
{
	vec3 temp;
	temp.x = (this->x)*scalar;
	temp.y = (this->y)*scalar;
	temp.z = (this->z)*scalar;	
	return temp;
}

vec3 vec3::operator / (const float& scalar) const
{
	vec3 temp;
	temp.x = this->x/scalar;
	temp.y = this->y/scalar;
	temp.z = this->z/scalar;	
	return temp;
}


std::string vec3::to_str()
{
	std::stringstream ss;
	ss << '(' << x << ',' << y << ',' << z << ')';
	return ss.str();
}

float dot(const vec3& u, const vec3& v)
{
	float dot = u.x*v.x + u.y*v.y; 
	return dot;	
}

vec3 cross(const vec3& u, const vec3& v)
{
	vec3 w;
	w.x = (u.y*v.z) - (v.y*u.z);
	w.y = (u.z*v.x) - (v.z*u.x);
	w.z = (u.x*v.y) - (v.x*u.y);
	return w;
}


float length(const vec3& u)
{
	return sqrt( (u.x*u.x) + (u.y*u.y) + (u.z*u.z) );
}

vec3 norm(const vec3& u)
{
	float l= length(u);
	vec3 n = u;
	n = n / l;
	return n;
}











/***********************************************************************
 * vec4
 **********************************************************************/

bool vec4::operator == (const vec4& other) const
{
	if(
		(this->x == other.x) && 
		(this->y == other.y) &&
		(this->z==other.z) &&
		(this->w==other.w)		
	)
		return true;
	else
		return false;
}

















/** Obsolete because I moved color storage to std::vector and this is
 * what this function was used for.
 *
 * 
vec2 std_vector_to_vec2(const std::vector<float>& v)
{
	vec3 t;
	t.x=v.at(0);
	t.y=v.at(1);
	return t;
}

vec3 std_vector_to_vec3(const std::vector<float>& v)
{
	vec3 t;
	t.x=v.at(0);
	t.y=v.at(1);
	t.z=v.at(2);
	return t;
}

*/











#endif
