#ifndef PARM_OBJ_HPP
#define PARM_OBJ_HPP

#include "vec.hpp"


/**
 * class Parm_obj 
 * 
 * This class is intended to be used as an object for passing to input 
 * and output functions. It's probably cheating and simulating global variables
 * 
 */



/// STL
#include <iostream>
#include <vector>

/// Project
#include "entities.hpp"
#include "models.hpp"





class Parm_obj 
{
	public:
	
	Parm_obj(){};


	int imsize_w=0;
	int imsize_h=0;	

	vec3 bg_color;


	// camera frame basis
	vec3 eye;
	vec3 view_dir;
	vec3 up_dir;
	float distance = 1.0; // distance to image plane (focal length)
	float aspect=1; // default
	vec3 u;
	vec3 v;
	vec3 w;
	vec3 n;

	std::vector<Sphere> sphere_v;
	std::vector<Ellipsoid> ellipsoid_v;
	std::vector<Mesh> mesh_v;
	std::vector<Light> light_v;
	std::vector<Spotlight> spotlight_v;
	
	// viewing window
	float fovv=90.0;
	float fovh=90.0;
	float view_width=1.0;
	float view_height=1.0;	
	vec3 ul,ur,ll,lr;	
	
	// map from viewing window to image pixels
	vec3 delta_h;
	vec3 delta_v;
	float deltaf_h=0;
	float deltaf_v=0;
	
	int max_color = 255; // 0 to 256-1
	
	// Container for our image data
	std::vector<int> pixel_values; 



	std::string comment = "#Assignment 1a";

	void print();

};






void Parm_obj::print()
{


	std::cout << "imsize_: "
	<< "(" << imsize_w << "," << imsize_h << ")"
	<< '\n';

	std::cout << "bg_color: "
	<< bg_color.to_str();

	std::cout << '\n';

//	std::cout << "mtl_color: ";
//	for(auto i : mtl_color)
//	{
//		std::cout 
//		<< i << " ";
//	}
//	std::cout << '\n';
	

	//std::vector<Sphere> sphere_v;
	//std::vector<Ellipsoid> ellipsoid_v;


	
	std::cout << "eye: " << eye.to_str() << '\n';
	std::cout << "view_dir: " << view_dir.to_str() << '\n';
	std::cout << "up_dir: " << up_dir.to_str() << '\n';		
	std::cout << "distance: " << distance << '\n';		
	std::cout << "aspect: " << aspect << '\n';	
	std::cout << "u: " << u.to_str() << '\n';
	std::cout << "v: " << v.to_str() << '\n';
	std::cout << "n: " << n.to_str() << '\n';	
	std::cout << "w: " << w.to_str() << '\n';




	// viewing window
	std::cout << "fovv: " << fovv << '\n';
	std::cout << "fovh: " << fovh << '\n';
	std::cout << "view_width: " << view_width << '\n';
	std::cout << "view_height: " << view_height << '\n';
	std::cout << "ul: " << ul.to_str() << '\n';
	std::cout << "ur: " << ur.to_str() << '\n';
	std::cout << "ll: " << ll.to_str() << '\n';
	std::cout << "lr: " << lr.to_str() << '\n';

	std::cout << "deltaf_h: " << deltaf_h << '\n';
	std::cout << "deltaf_v: " << deltaf_v << '\n';
	std::cout << "delta_h: " << delta_h.to_str() << '\n';
	std::cout << "delta_v: " << delta_v.to_str() << '\n';



	std::cout << "Sphere list: " << '\n';
	for(auto i : sphere_v)
	{
		std::cout << '\t'
		<< "(" << i.origin.x << "," << i.origin.y << "," << i.origin.z << "," << i.r << ")" << '\n';
	}

	
	std::cout << "Ellipsoid list: " << '\n';
	for(auto i : ellipsoid_v)
	{
		std::cout << '\t'
		<< "(" << i.x << "," << i.y << "," << i.z << "," << i.rx << "," << i.ry << "," << i.rz << ")" << '\n';
	}









	for(auto x: sphere_v)
		x.print();
	for(auto x: ellipsoid_v)
		x.print();
	for(auto x: mesh_v)
		x.print();

	std::cout << "Light list: " << '\n';
	for(auto x: light_v)
		x.print();
	for(auto x: spotlight_v)
		x.print();



	
}




















#endif
