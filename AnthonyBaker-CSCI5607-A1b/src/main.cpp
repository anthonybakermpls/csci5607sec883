
/**
 * This program (executable name: assignment1b) reads in an ascii text file 
 * which contains a specified input consisting of a keywords specifing 
 * various properties of the viewing space and output image.
 * The program then generates and writes a file in the required 
 * PPM format with the dimensions as read in from the input file.
 *
 * The basic steps are:
 * - define viewing parameters
 * - define the scene
 * - define equation of each ray and
 * - determine if/where a ray intersects an object
 * 
 * 
 * Usage:
 * ./assignment1b inputfile.txt
 * ./assignment1b ../inputfile.txt; display ../input.ppm
 * 
 * 
 * TODO:
 * - Exceptions: using these is new for me so continually make sure it's being done right.
 * - Too many parm_objs. Need to make a decision about moving parm_obj into an entity class of some kind
 * - Distance is whatever we want it to be
 *
 * Notes:
 * When you're banging your head against the wall because the thing you're trying to fix still doesn't work even though you've checked it million times,
 * LOOK SOMEWHERE ELSE
 */



// std
#include <algorithm> //min max
#include <chrono>
#include <complex>
#include <cmath>
#include <exception>
#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <random> // random number device
#include <vector>

// project
#include "entity.hpp"
#include "input_file_parser.hpp"
#include "parm_obj.hpp"
#include "models.hpp"
#include "util.hpp"





// Function declarations
void compute_viewing_parms(Parm_obj&);
void compute_pixels(Parm_obj&);
void create_viewing_ray(Parm_obj&, Ray&, const int& i, const int& j);
vec3 trace_ray(Parm_obj&, const Ray&);
vec3 shade_ray(Parm_obj&, const Ray&, const float&, const Sphere&);
void write_output(const std::string&, const Parm_obj& );
int denormalize_color(const float& , const int& );



int main( int argc, char **argv )
{
	// Timer for measuring performance
	auto start = std::chrono::steady_clock::now();


	try
	{
		// Make sure the user specified an input file. If not, tell them how to do so.
		if( argc < 2 )
		{
			throw std::runtime_error ("No input file provided. Usage: e.g. \"./example ../examplefile.txt\"");
		}

		std::string input_filename=argv[1];
		Parm_obj parm_obj; // Contains many of our variables and is used for passing through functions. Refactor when necessary
		Input_file_parser input_file_parser; // Handles reading input files and filling parameters
		input_file_parser.read_input(input_filename, parm_obj); // Read input file
		compute_viewing_parms(parm_obj);
		parm_obj.print();
		compute_pixels(parm_obj);
		std::string output_filename = input_filename.substr(0, input_filename.size() - 4) + ".ppm";	// filename is something like blah.txt and we want it to be blah.ppm
		write_output(output_filename, parm_obj);

	}
	catch (std::exception& e) 
	{
		std::cerr << "Exception: " << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	catch (const std::string& s) 
	{
		std::cerr << "Exception: " << s << std::endl;
		exit(EXIT_FAILURE);
	}
	//catch (std::ios_base::failure& e)
	//{
	//	std::cerr << "failbit | badbit" << std::endl;
	//	//exit(EXIT_FAILURE);		
	//}	
    catch(...) // Catch anything else.
    {
		std::cerr << "General exception." << std::endl;
		exit(EXIT_FAILURE);
    }
    
    
	// Timer for measuring performance
	auto end = std::chrono::steady_clock::now();			
	auto diff = end - start;
	std::cout << "Program duration: " << (std::chrono::duration <double, std::ratio<60>> (diff).count()) << " min" << std::endl;

	return 0;
}




















/** 
 * This function takes care of setting up all viewing parameters that are
 * not given in our input file
 **/
void compute_viewing_parms(Parm_obj& parm_obj)
{

	// compute u
	parm_obj.u = norm( cross(parm_obj.view_dir,parm_obj.up_dir) );

	// compute v
	parm_obj.v = norm( cross(parm_obj.u, parm_obj.view_dir) );

	// compute n
	parm_obj.n= norm(parm_obj.view_dir);

	// compute w
	parm_obj.w= parm_obj.n*(-1.f); // oppopse of n or the view direction

	
	// compute viewing window w,h in world coordinate units
	// parm_obj.view_w = 2*distance* (tan(fovh/2)); // do this below based off of h and aspect
	parm_obj.view_height = 2 * (parm_obj.distance) *  tan( (parm_obj.fovv/2) * (PI/180.0) ) ;	// CMATH TAN IS IN RADIANS

	
	// compute aspect ratio given image/view window size
	parm_obj.aspect= (float)parm_obj.imsize_w/(float)parm_obj.imsize_h;

	if(parm_obj.aspect == 1 )
	{
		std::cout << "Aspect ratio is 1." << std::endl;
		parm_obj.view_width=parm_obj.aspect*parm_obj.view_height;
	}
	
	// compute corners of viewing window in world coordinate space
	parm_obj.ul =  (parm_obj.eye + parm_obj.n*parm_obj.distance) + parm_obj.v* (float)parm_obj.view_height/2.f - parm_obj.u* (float)parm_obj.view_width/2.f;
	parm_obj.ur =  (parm_obj.eye + parm_obj.n*parm_obj.distance) + parm_obj.v* (float)parm_obj.view_height/2.f + parm_obj.u* (float)parm_obj.view_width/2.f;
	parm_obj.ll =  (parm_obj.eye + parm_obj.n*parm_obj.distance) - parm_obj.v* (float)parm_obj.view_height/2.f - parm_obj.u* (float)parm_obj.view_width/2.f;
	parm_obj.lr =  (parm_obj.eye + parm_obj.n*parm_obj.distance) - parm_obj.v* (float)parm_obj.view_height/2.f + parm_obj.u* (float)parm_obj.view_width/2.f;

	// compute delta  h,v (map from image size to view window)
	parm_obj.delta_h= (parm_obj.ur - parm_obj.ul)/((float)parm_obj.imsize_w-1);
	parm_obj.delta_v= (parm_obj.ll - parm_obj.ul)/((float)parm_obj.imsize_h-1);
	
}




/**
 * for each pixel
 * Compute ray equation
 * compute viewing ray
 * find 1st object hit by ray and its surface normal n
 * set pixel color to value computed from hit point, light, and n
 */
void compute_pixels(Parm_obj& parm_obj)
{
	/// TODO: Change so we don't push the entire bg and then overwrite - do each pixel only once.

	// push back the full image worth of rgb zeros

	for(int i=0; i< parm_obj.imsize_w*parm_obj.imsize_h; i++)// 3 rgb values (in 0-max_color range) per pixel
	{ 
		parm_obj.pixel_values.push_back( denormalize_color(parm_obj.bg_color.x , parm_obj.max_color ) ); 
		parm_obj.pixel_values.push_back( denormalize_color(parm_obj.bg_color.y , parm_obj.max_color ) ); 
		parm_obj.pixel_values.push_back( denormalize_color(parm_obj.bg_color.z , parm_obj.max_color ) ); 
	}
	
	// use i as horizontal image pixel dimension and j as vertical image pixel dimension
	// but in row-major order so i is in innner loop...
	
	float vec_element=0; // used for std::vector element location
	vec3 color;
	Ray ray; // our primary viewing ray that we'll reuse
	
	for(int j=0; j<parm_obj.imsize_h; j++) //  move down
	{	
		for (int i =0; i<parm_obj.imsize_w; i++) //  move across
		{
			create_viewing_ray(parm_obj, ray, i,j);
			color = trace_ray(parm_obj, ray); // do the ray/object intersection and pixel value storage

			// we are at pixel i,j. This translates to location in a serial vector element list of
			vec_element = (j*parm_obj.imsize_w + i )* 3; // multiply by three because we have 3 values per pixel
			
			// Set vector elements. Need to denormalize our colors to parm_obj.max_color	
			parm_obj.pixel_values.at(vec_element) =  denormalize_color(color.x,parm_obj.max_color);
			parm_obj.pixel_values.at(vec_element+1) =  denormalize_color(color.y,parm_obj.max_color);
			parm_obj.pixel_values.at(vec_element+2) =  denormalize_color(color.z,parm_obj.max_color);
		}
	}

}



void create_viewing_ray(Parm_obj& parm_obj, Ray& ray, const int& i, const int& j)
{
	// ray equation is r(t) = e + t(p - e)
	
	ray.origin = parm_obj.eye;
	vec3 p;
	vec3 p_minus_e;
	p = parm_obj.ul + ( parm_obj.delta_h * i) + ( parm_obj.delta_v * j);
	p_minus_e = p - parm_obj.eye;
	
	//parm_obj.ray.direction= norm(p_minus_e); // normalize
	ray.direction= norm(p_minus_e); // normalize
}





/**
 *  The bulk of the work
 */
vec3 shade_ray(Parm_obj& parm_obj, const Ray& ray,  const float& t, const Sphere& s)
{

	vec3 V; // to eye
	vec3 L; // to light
	vec3 N; // surface normal
	vec3 H; // half way between light L and eye V ( v+l/(norm(v+l)) )

	vec3 return_color; // The default is our object's diffuse
	int shadow_flag=1;
	Ray shadow_ray;

	float Ir=0;
	float Ig=0;
	float Ib=0;

	float N_dot_L=0;
	float N_dot_H=0;
	float N_dot_H_To_Nth=0;


	vec3 intersection = (ray.direction - ray.origin)*t + ray.origin; // point at which our ray intersects with object

	// Determine surface normal N
	N = s.get_normal(intersection); 

	// Determine the direction to the viewer V
	V = norm(parm_obj.eye - intersection);

	shadow_ray.origin = intersection;
	vec3 shadow_ray_intersection; // intersection between shadow ray and object in scene

	vec3 kd_od = s.material.Od * s.material.kd;
	vec3 ks_os = s.material.Os * s.material.ks;


	for(unsigned int i=0; i< parm_obj.light_v.size(); i++)
	{
		shadow_flag = 1; // reset to a factor of 1
		
		// Determine L
		if (parm_obj.light_v.at(i).w == 0) // Directional light source (the origin is used to hold the direction components)
			L = norm(parm_obj.light_v.at(i).origin *-1);
			
		else if (parm_obj.light_v.at(i).w ==1) // point/ positional
			L = norm(parm_obj.light_v.at(i).origin - intersection);


		// Determine the direction of the halfway vector H
		H = norm(V + L);

		N_dot_L = std::max( 0.f, dot(N, L));
		N_dot_H = std::max( 0.f, dot(N, H));
		N_dot_H_To_Nth = pow( N_dot_H, s.material.n);

		/** Shadow computations
		 * Check the performance of this
		 */

		// Directional light source
		if (parm_obj.light_v.at(i).w == 0) 
		{
			shadow_ray.direction = parm_obj.light_v.at(i).origin * -1;
			float shadow_t=0;
			for (unsigned int i = 0; i < parm_obj.sphere_v.size(); i++)			// check all spheres in scene
			{
				if ( parm_obj.sphere_v.at(i).hit( shadow_ray, 0, 0, shadow_t) )
				{
					if(shadow_t>0)
					shadow_flag =0; // In shadow: set factor to 0 which will cancel color values
				}
			}	
		}
		// point/ positional
		else if (parm_obj.light_v.at(i).w ==1) 
		{
			shadow_ray.direction = parm_obj.light_v.at(i).origin - intersection; // note this is different for the directional light
			float shadow_t=0;
			for (unsigned int i = 0; i < parm_obj.sphere_v.size(); i++)			// check all spheres in scene
			{
				if ( parm_obj.sphere_v.at(i).hit( shadow_ray, 0, 0, shadow_t) )
				{
					if(shadow_t>0 && shadow_t <1)
					shadow_flag =0; // In shadow: set factor to 0 which will cancel color values
				}
			}
		}

		// compute the values componentwise

 		Ir= Ir + (shadow_flag * parm_obj.light_v.at(i).intensity.x) * (
		clamp((kd_od.x *  N_dot_L),0.f,1.f) + clamp((ks_os.x *  N_dot_H_To_Nth),0.f,1.f)
		);
		
		Ig= Ig + (shadow_flag * parm_obj.light_v.at(i).intensity.y) * (
		clamp((kd_od.y *  N_dot_L),0.f,1.f) + clamp((ks_os.y *  N_dot_H_To_Nth),0.f,1.f)
		);

		Ib= Ib + (shadow_flag * parm_obj.light_v.at(i).intensity.z) * (
		clamp((kd_od.z *  N_dot_L),0.f,1.f) + clamp((ks_os.z *  N_dot_H_To_Nth),0.f,1.f)
		);

	}

	// Add ambient terms
	Ir = Ir + (s.material.ka * s.material.Od.x);
	Ig = Ig + (s.material.ka * s.material.Od.y);
	Ib = Ib + (s.material.ka * s.material.Od.z);

 	return_color.x = Ir;
 	return_color.y = Ig;
 	return_color.z = Ib; 	 	

	return return_color;
}








/**
 * For each object in our scene, check to see if the given ray intersects it.
 * TODO: Refactor this.
 */
vec3 trace_ray(Parm_obj& parm_obj, const Ray& ray)
{
	vec3 return_color = parm_obj.bg_color;

	float min_t=99999; // this is the minimum t out of all "hits" we will overwrite on the pixel if current t is less than this	
	float t = min_t; // reset this for our new ray


	Sphere closest_sphere;
	
	// check all spheres in scene	
	for (unsigned int i = 0; i < parm_obj.sphere_v.size(); i++)
	{
		if ( parm_obj.sphere_v.at(i).hit( ray, 0, 0, t) )
		{
			if(t < min_t)
			{
				min_t = t;
				closest_sphere=parm_obj.sphere_v.at(i);
				//std::cout << "On Ray: " << parm_obj.ray.direction.to_str() << std::endl;
				//std::cout << "Closest sphere = parm_obj.sphere_v.at(" << i << ") with t at " << parm_obj.t << std::endl;
			}
		}
	}

	if(min_t < 99999) // there was a sphere hit in our range
		return_color = shade_ray(parm_obj, ray, t, closest_sphere); // change from closest_sphere to closest_model

	return return_color;
}









/**
 * Write the ouptut image file
 */
void write_output(const std::string& filename, const Parm_obj& parm_obj)
{
	// build a PPM header
	// Format of our input file is keyword "imgsize" and then two integers specifiying the dimension
	// Format of output is a magic number, comment, dimensions, color range, and then our pixel values in RGB tuples.	
	std::string magic_number = "P3";

	std::string ppm_header(
	magic_number+"\n"
	+parm_obj.comment+"\n"
	+std::to_string(parm_obj.imsize_w)+" "+std::to_string(parm_obj.imsize_h)+"\n"
	+std::to_string(parm_obj.max_color)+"\n"
	);

	// Create a filestream for outputting the text file, and open it. Quit program if there's an error
	std::ofstream outfile( filename.c_str() );
	if( !outfile.is_open() )
	{ std::cerr << "**Error: Could not open output file " << filename << std::endl; exit(1); }
	
	// Write the header
	outfile << ppm_header; // last item already has a newline


	// Write the pixel RGB values
	std::cout << "Writing " << filename << '\n';
	int line_length=0; // allow us to write more than one value per line
	for(auto i : parm_obj.pixel_values)
	{
		outfile << i;
		line_length++;
		if(line_length >14)    // 3 digts + 1 space * 3 components = 12 bytes max size of one pixel (5 pixels * 12 bytes = 60)
		{
			line_length=0;
			outfile << '\n';
		}
		else{
			outfile << " ";
		}
	}
}



int denormalize_color(const float& f, const int& max_denorm_color )
{
	return floor(f == 1.0 ? max_denorm_color : (int) (f * (float)(max_denorm_color + 1)) );
}


