#ifndef UTIL_HPP
#define UTIL_HPP



#include "vec.hpp"


/** Misc. constants, functions whatever
 */



#define PI 3.14159265
//*PI/180.0




/** Clamp a float to an interval */
float clamp(const float& f, const float& min, const float& max)
{
	if(f < min)
		return min;
	else if (f > max)
		return max;
	else if (f == 0 )
	{
		//std:: cout << "found -0" << std::endl;
		return abs(f); // remove any negative 0
	}
	return f;
}


/** Clamp a vec3 to a float interval */
vec3 clamp(const vec3& f, const float& min, const float& max)
{
	vec3 temp =f;
	
	if(temp.x < min)
		temp.x = min;
	else if (temp.x > max)
		temp.x = max;

	if(temp.y < min)
		temp.y = min;
	else if (temp.y > max)
		temp.y = max;

	if(temp.z < min)
		temp.z = min;
	else if (temp.z > max)
		temp.z = max;

	return temp;
}


















#endif
