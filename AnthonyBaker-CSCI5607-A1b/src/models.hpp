#ifndef MODELS_HPP
#define MODELS_HPP





/**
 * This file will hold the classes derived from Model which are actual things to be drawn and shoul implement the Model functions
 */






/// sTL
#include <vector>

/// Project
#include "entity.hpp"
#include "entities.hpp"
#include "model.hpp"
#include "vec.hpp"








class Sphere : public Model
{
	public:
	Sphere(){};
	void print(){};
	bool hit(const Ray&, const float&, const float&, float&);
	vec3 get_normal(const vec3&) const;	
	Box bounding_box(){return Box();};
	
	float r=0;
	Material material; // move away from std::vector
};

bool Sphere::hit(const Ray& ray, const float& t0, const float& t1, float& t)
{

	float A=0.0;
	float B=0.0;
	float C=0.0;
	float discriminant=0;

	float ts1=0; // used in the 2 solution case
	float ts2=0; // used in the 2 solution case

	// A = std::pow(ray.direction.x,2) + std::pow(ray.direction.y,2) + std::pow(ray.direction.z,2) ; // non-normalized direction version
	A = 1;		// Because this is normalized, A=1  // Don't use this if ray is not normalized.
	B = 2*( ray.direction.x* (ray.origin.x - origin.x)
		+   ray.direction.y* (ray.origin.y - origin.y)
		+   ray.direction.z* (ray.origin.z - origin.z) );
	C = pow(ray.origin.x - origin.x,2)
		+ pow(ray.origin.y - origin.y,2)
		+ pow(ray.origin.z - origin.z,2)
		- pow(r,2);
	
	// compute discriminant B^2 - 4AC
	discriminant = B*B - 4*A*C;
	
	if ( discriminant < 0 ) // no solution
	{return false;}

	else if ( discriminant == 0 )	// 1 solution
	{
		// parameter value of intersection
		t = (-B)/(2*A); // discriminant zero

		if(t>0)  // In front of eye
			return true;
	}

	else if ( discriminant > 0 ) // two solutions - 
	{
		// Which t value is closer to eye?
		ts1 = (-B + sqrt(discriminant))/(2*A);			
		ts2 = (-B - sqrt(discriminant))/(2*A);

		if( (ts1 >0) && (ts1<ts2) )
			{t = ts1;}
		else if(ts2>0 && ts2<ts1)
			{t =ts2;}

		if(t>0) // In front of eye
			return true;
	}

	return false; // dummy return


	
}

vec3 Sphere::get_normal(const vec3& p) const
{
	vec3 temp;
	// for a sphere the normal at a point p is
	temp = (p - origin)/r;
	return norm(temp);
}



	
//Box Sphere::bounding_box(){return Box();};














class Ellipsoid : public Model
{
	public:
	Ellipsoid(){};
	void print(){};
	bool hit(const Ray&, const float&, const float&,  float&);
	vec3 get_normal(const vec3&) const;	
	Box bounding_box(){return Box();};
		
	// transition this to base class origin		
	float x=0;
	float y=0;
	float z=0;
	float rx=0;
	float ry=0;
	float rz=0;
	Material material; // move away from std::vector	
};
bool Ellipsoid::hit(const Ray& ray, const float& t0, const float& t1,  float& t)
{

	return false;
}


vec3 Ellipsoid::get_normal(const vec3& p) const
{
	vec3 temp;
	// Define this
	//temp = (p - origin)/r;
	return temp;
}















/** Future use
 */
class Face
{
	public:
	Face(){};
	void print(){};
			
	int v1;
	int v2;
	int v3;
};





class Mesh : public Model
{
	public:
	Mesh(){};
	void print(){};
	bool hit(const Ray&, const float&, const float&, float& t);
	vec3 get_normal(const vec3&) const;			
	Box bounding_box(){return Box();};
				
	std::vector<vec3> verts;
	std::vector<Face> faces;
};
bool Mesh::hit(const Ray& ray, const float& t0, const float& t1, float& t)
{

	return false;
}

vec3 Mesh::get_normal(const vec3& p) const
{
	vec3 temp;
	// define this 
	// temp = (p - origin)/r;
	return temp;
}






















/*
	// check all Ellipsoids in scene
	for( auto current_ellipsoid : parm_obj.ellipsoid_v)
	{
		A = std::pow(ray.direction.x,2)/current_ellipsoid.rx + std::pow(ray.direction.y,2)/current_ellipsoid.ry + std::pow(ray.direction.z,2)/current_ellipsoid.rz ;
		// Because this is normalized, A=1  // Don't use this if ray is not normalized.
		//A = 1;
		B = 2*( ray.direction.x* (ray.origin.x - current_ellipsoid.x)/current_ellipsoid.rx
			+   ray.direction.y* (ray.origin.y - current_ellipsoid.y)/current_ellipsoid.ry
			+   ray.direction.z* (ray.origin.z - current_ellipsoid.z)/current_ellipsoid.rz );
		C = pow(ray.origin.x-current_ellipsoid.x,2)/current_ellipsoid.rx
			+ pow(ray.origin.y-current_ellipsoid.y,2)/current_ellipsoid.ry
			+ pow(ray.origin.z-current_ellipsoid.z,2)/current_ellipsoid.rz
			- 1;
		
		// compute discriminant B^2 - 4AC
		discriminant = B*B - 4*A*C;

		if ( discriminant < 0 ) // no solution
		{}
		else if ( discriminant == 0 )	// 1 solution
		{

			// parameter value of intersection
			closer_t = (-B)/(2*A); // discriminant zero
			
			if(closer_t < min_t)// this is the minimum t out of all "hits" we will overwrite on the pixel if current t is less than this
			{
				closest_ellipsoid_ptr=&current_ellipsoid;
				min_t =closer_t;					
				return_color= shade_ray(current_ellipsoid.mat_color,closer_t);								

			}					
		}
		else if ( discriminant > 0 ) // two solutions - Fix later to figure out which one
		{
			// parameter value of intersection
			// Which t value is closer to eye?
			t1 = (-B + sqrt(discriminant))/(2*A);
			t2 = (-B - sqrt(discriminant))/(2*A);

			if(t1 >0 && t1<t2){closer_t = t1;}
			else if(t2>0 && t2<t1){closer_t=t2;}		
			
			if(closer_t < min_t)// this is the minimum t out of all "hits" we will overwrite on the pixel if current t is less than this
			{
				closest_ellipsoid_ptr=&current_ellipsoid;
				min_t =closer_t;					
				return_color= shade_ray(current_ellipsoid.mat_color, closer_t);
			}								
		}
	}
*/



#endif
