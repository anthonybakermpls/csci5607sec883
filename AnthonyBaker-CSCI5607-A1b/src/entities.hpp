#ifndef ENTITIES_HPP
#define ENTITIES_HPP




/**
 * Entities are directly derived from Entity. There are not "models" in
 * that they are not objects that appear in a scene exactly.
 */


/// STL
#include <vector>


/// PRoject
#include "entity.hpp"
#include "vec.hpp"




class Ray : public Entity
{
	public:
	Ray(){};
	void print(){};

	vec3 direction;
};

class Light : public Entity
{
	public:
	Light(){};
	void print();
		
	float w;
	// if the light is directional, the origin will actually mean which direction it is casting light rays
	
	vec3 intensity;
};
void Light::print()
{

	std::cout << origin.to_str() << " + w: " << w << std::endl;
}

class Spotlight : public Entity
{
	public:
	Spotlight(){};
	void print(){};

	float w;
	vec3 dir;
	float theta;
	
	vec3 intensity;
};
















/** future use
 */
class Eye_coord_frame : public Entity
{
	public:
	Eye_coord_frame(){};	
	void print();
		
	vec3 u;
	vec3 v;
	vec3 w;
	vec3 n;
};

class World_coord_frame : public Entity
{
	public:
	World_coord_frame(){};
	void print();
		
	vec3 u;
	vec3 v;
	vec3 w;
	vec3 n;
};







#endif
