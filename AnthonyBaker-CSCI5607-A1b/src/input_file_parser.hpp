#ifndef INPUT_FILE_PARSER_HPP
#define INPUT_FILE_PARSER_HPP





/**
 * class Input_file_parser
 * 
 * At some point we can make this super generic that can handle files 
 * based on a file extension or a spec or something.
 */




/// stdlib includes
#include <string>
#include <vector>

/// project includes
#include "entity.hpp"
#include "parm_obj.hpp"
#include "vec.hpp"
#include "util.hpp"




class Input_file_parser
{
public:
	Input_file_parser(){}
	void read_input(const std::string& , Parm_obj&);
};





void Input_file_parser::read_input(const std::string& filename, Parm_obj& parm_obj)
{

	// Are there any input file name requirements? I guess we can at least check the extension.
	if (filename.size() < 5)
	{throw std::runtime_error ("Malformed file name");}

	std::string ext = filename.substr(filename.size() -4);

	if ( ext != ".txt") 
	{throw std::runtime_error ("Incorrect file extension.");}

	// Create a filestream for reading the text file, and open it.
	std::ifstream inputfile_ifs;

	/// TODO figure out this exception
	//inputfile_ifs.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
	//try{

	inputfile_ifs.open( filename.c_str() );
	if( !inputfile_ifs.is_open() )
	{throw std::runtime_error ("Could not open file." + filename);}


	// Read input file and try to handle bad cases
	std::string var="";
	std::string line="";
	
	// do one getline and if we see eof then the file is empty.
	std::getline( inputfile_ifs, line );
	if(inputfile_ifs.eof())
	{ 
		throw std::runtime_error ("File empty.");
	}
	else  // clear eof bit and rewind
	{
		inputfile_ifs.clear();
		inputfile_ifs.seekg(0, inputfile_ifs.beg);
	}

	bool got_eye=false;
	bool got_viewdir=false;
	bool got_updir=false;
	bool got_fovv=false;
	bool got_imsize=false;
	bool got_bkgcolor=false;
	

	


	// These are local copes that are here to be filled by the while loop and then inserted into a container when full

	Material material;// a local copy that will be used for every shape and only changed when a new mtlcolor line is found
	float Odr=-1;
	float Odg=-1;
	float Odb=-1;
	float Osr=-1;
	float Osg=-1;
	float Osb=-1;
	float ka =-1;
	float kd =-1;	
	float ks =-1;
	int n  =-1;

	

	vec3 v;
	Face f;
	bool first_f;
	int count_tris;	
	Mesh mesh;

	int line_count=0;

	while( std::getline( inputfile_ifs, line ) )
	{
		line_count++;
		
		var="";
		std::stringstream ss;
		
		if(line.length() != 0) // allow blank lines
		{
			ss << line;
			ss >> var;
		}
		
		// We can't use switch on string types so it has to be an if else chain


		
		if( var == "eye" )
		{
			got_eye=true;
			ss >> parm_obj.eye.x; 
			ss >> parm_obj.eye.y; 
			ss >> parm_obj.eye.z;
		}
		
		else if( var == "viewdir" )
		{
			got_viewdir=true;
			ss >> parm_obj.view_dir.x; 
			ss >> parm_obj.view_dir.y; 
			ss >> parm_obj.view_dir.z; 	
			if( (length(parm_obj.view_dir) != 1) )
			{
				// don't throw just warn and normalize
				//throw std::runtime_error ("Input vector not unit length: viewdir.");}
				std::cerr << "Warning: input vector viewdir not unit length. Normalizing." << std::endl;
				parm_obj.view_dir= norm( clamp(parm_obj.view_dir,0.f,1.f) );
			}
		}

		else if( var == "updir" )
		{
			got_updir=true;
			ss >> parm_obj.up_dir.x; 
			ss >> parm_obj.up_dir.y; 
			ss >> parm_obj.up_dir.z; 	
			if( (length(parm_obj.up_dir) != 1) )
			{

				// don't throw just warn and normalize
				//throw std::runtime_error ("Input vector not unit length: updir.");
				std::cerr << "Warning: input vector updir not unit length. Normalizing." << std::endl;
				parm_obj.view_dir= norm( clamp(parm_obj.up_dir,0.f, 1.f) );
			}	
			
		}

		else if( var == "fovv" )
		{
			got_fovv=true;
			ss >> parm_obj.fovv; 
			if( (parm_obj.fovv <0) || (parm_obj.fovv >180) )
			{throw std::runtime_error ("fovv out of range.");}				
		}

		else if( var == "imsize" )
		{
			got_imsize=true;
			ss >> parm_obj.imsize_w >> parm_obj.imsize_h;
			if( (parm_obj.imsize_w <1) || (parm_obj.imsize_h <1) )
			{throw std::runtime_error ("image size out of range.");}				
		}

		else if( var == "bkgcolor" )
		{
			got_bkgcolor=true;
			float r;
			float g;
			float b;						
			ss >> r;ss >> g;ss >> b;
				
			if(
				(r <0) || (r >1) ||
 				(g <0) || (g >1) ||
				(b <0) || (b >1)
			)
			{throw std::runtime_error ("bkgcolor out of range.");}	

			// we only set this once so that is why we do push_back instead of at() like in the mat_color situation
			parm_obj.bg_color.x= r; 
			parm_obj.bg_color.y=g;
			parm_obj.bg_color.z=b;				
		}

		
		// This keyword lines can apppear any number of times and sets the color for objects after until
		// another mtlcolor is called.
		else if( var == "mtlcolor" ) // only change the local copy
		{
			// TODO: make sure all are read in properly
			ss >> Odr;
			ss >> Odg;
			ss >> Odb;
			ss >> Osr;
			ss >> Osg;
			ss >> Osb;
			ss >> ka ;
			ss >> kd ;			
			ss >> ks ;
			ss >> n  ;

			// range checking
			if(
				(Odr <0 || Odr >1) || 
				(Odg <0 || Odg >1) || 
				(Odb <0 || Odb >1) || 
				(Osr <0 || Osr >1) || 
				(Osg <0 || Osg >1) || 
				(Osb <0 || Osb >1) || 
				(ka  <0 || ka >1) ||
				(kd  <0 || kd >1) || 				
				(ks  <0 || ks >1) || 
				(n   <0 || n >1000)
			)
			{throw std::runtime_error ("mtlcolor missing or out of range. Found on line " + std::to_string(line_count));}	

			material.Od.x = Odr;
			material.Od.y = Odg;
			material.Od.z = Odb;

			material.Os.x = Osr;
			material.Os.y = Osg;
			material.Os.z = Osb;								

			material.ka = ka;
			material.kd = kd;			
			material.ks = ks;
			material.n = n;
		}

		
		// These keyword lines can apppear any number of times
		else if( var == "sphere" )
		{
			Sphere s;
			ss >> s.origin.x >> s.origin.y >> s.origin.z >> s.r;
			s.material = material; // set to the current material
			parm_obj.sphere_v.push_back(s);
		}
				
		else if( var == "ellipsoid" )
		{
			Ellipsoid e;
			ss >> e.x >> e.y >> e.z >> e.rx >> e.ry >> e.rz;
			e.material = material; // set to the current material
			parm_obj.ellipsoid_v.push_back(e);
		}


		// The mesh input block.
		// I don't care for this logic but the way our input is defined there's not much help.
		else if( var == "v" )
		{
			// This begins a mesh definition beginning
			// We could possibly call ourselves recursively or for now just do some dumb logic

			ss >> v.x; ss >> v.y; ss >> v.z; // pull in the v
			mesh.verts.push_back(v); // store
		}
		else if( var == "f" )
		{
			if(first_f==true) // first instance of an f after the v's
			{
				// calculate how many verts/trianges we have accumulated just before this
				// 3+(n-1) = 2+n verts for n tris
				// n tris have n faces
				count_tris = mesh.verts.size()-2;
				// one face per tri so we can expect count_tris f's
				first_f=false;
			}
		
			ss >> f.v1; ss >> f.v2; ss >> f.v3; // pull in the f
			mesh.faces.push_back(f); // store
			mesh.material = material;// set to the current material
			
			count_tris--;
			if(count_tris==0) // we have all of our face information
				parm_obj.mesh_v.push_back(mesh); // store the local temp mesh into our program

			// clear the local for reuse
			mesh.verts.clear();
			mesh.faces.clear();
		}

		else if( var == "light" )
		{
			Light light;

			ss >> light.origin.x;
			ss >> light.origin.y;
			ss >> light.origin.z;
			ss >> light.w;

			ss >> light.intensity.x;
			ss >> light.intensity.y;
			ss >> light.intensity.z;

			clamp(light.intensity, 0.f, 1.f);
			
			parm_obj.light_v.push_back(light); // store permanently
		}
		/*
		else if( var == "spotlight" )
		{
			Spotlight spotlight;

			ss >> spotlight.origin.x;
			ss >> spotlight.origin.y;
			ss >> spotlight.origin.z;
			ss >> spotlight.w;

			ss >> spotlight.color.x;
			ss >> spotlight.color.y;
			ss >> spotlight.color.z;

			clamp(spotlight.color, 0.f, 1.f);

			parm_obj.light_v.push_back(spotlight); // store permanently	
		}
		*/
		else
		{}
		

	}// end while getline loop

	
	if (inputfile_ifs.bad()) /// TODO figure out how to use exceptions for this ios_base
	{ 
		std::cerr << "**Error: Read/writing error on i/o operation. " << filename << std::endl; 
		exit(1); 
	}

		



	/** Do any checks that require comparison between input values or that require more than one value to compare or any general limits.
	*/


	// first check that we actually got all required parameters
	if(
	(got_eye==false)		||
	(got_viewdir==false)	|| 
	(got_updir==false)		||
	(got_fovv==false)		||
	(got_imsize==false)		||
	(got_bkgcolor==false)
	)
	{
		// I think throwing is appropriate here
		throw std::runtime_error ("Missing input.");
		//std::cerr << "Warning: Missing input. Using defaults" << std::endl;
	}










	// view and up cannot be parallel
	vec3 temp=cross(parm_obj.view_dir,parm_obj.up_dir);
	if( (temp.x==0) && (temp.y==0) && (temp.z==0) )
	{throw std::runtime_error ("Input view and up vectors are parallel.");}	


	// imsize constraints
	if( 
		   (parm_obj.imsize_w == 0)
		|| (parm_obj.imsize_h == 0)
		|| (parm_obj.imsize_w < 0) 
		|| (parm_obj.imsize_h < 0)
		|| (parm_obj.imsize_w > 4000) // 4k enough? 
		|| (parm_obj.imsize_h > 4000)
	)
	{throw std::runtime_error ("Failed image dimensions.");}	



}



#endif
