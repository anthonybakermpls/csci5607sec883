#ifndef PARM_OBJ_HPP
#define PARM_OBJ_HPP

#include "vec.hpp"


/**
 * class Parm_obj 
 * 
 * This class is intended to be used as an object for passing to input 
 * and output functions.
 * 
 * At some point this  broken up into modules for various 
 * concepts.
 */




#include <iostream>
#include <vector>
#include "model.hpp"


class Parm_obj 
{
	public:
		Parm_obj(){};


	int imsize_w=0;
	int imsize_h=0;	

	std::vector<float> bg_color;
	//std::vector<float> mtl_color; // unused here



	// camera frame basis
	vec3 eye;
	vec3 view_dir;
	vec3 up_dir;
	float distance = 1.0; // distance to image plane (focal length)
	float aspect=1; // default
	vec3 u;
	vec3 v;
	vec3 w;
	vec3 n;


	std::vector<Sphere> sphere_v;
	std::vector<Ellipsoid> ellipsoid_v;


	// viewing window
	float fovv=90.0;
	float fovh=90.0;
	float view_w=1.0;
	float view_h=1.0;	
	vec3 ul,ur,ll,lr;	
	
	// map from viewing window to image pixels
	vec3 delta_h;
	vec3 delta_v;
	float deltaf_h;
	float deltaf_v;
	
	int max_color = 255; // 0 to 256-1
	
	// Container for our image data
	std::vector<int> pixel_values; 

	std::string comment = "#Assignment 1a";

	void print();

};





/**
 * I don't really like this duplication but i'm not sure there's a way
 * around it
 */
void Parm_obj::print()
{


	std::cout << "imsize_: "
	<< "(" << imsize_w << "," << imsize_h << ")"
	<< '\n';

	std::cout << "bg_color: ";
	for(auto i : bg_color)
	{
		std::cout 
		<< i << " ";
	}
	std::cout << '\n';

//	std::cout << "mtl_color: ";
//	for(auto i : mtl_color)
//	{
//		std::cout 
//		<< i << " ";
//	}
//	std::cout << '\n';
	

	//std::vector<Sphere> sphere_v;
	//std::vector<Ellipsoid> ellipsoid_v;


	
	std::cout << "eye: " << eye.to_str() << '\n';
	std::cout << "view_dir: " << view_dir.to_str() << '\n';
	std::cout << "up_dir: " << up_dir.to_str() << '\n';		


	std::cout << "Sphere list: " << '\n';
	for(auto i : sphere_v)
	{
		std::cout << '\t'
		<< "(" << i.x << "," << i.y << "," << i.z << "," << i.r << ")" << '\n';
	}

	
	std::cout << "Ellipsoid list: " << '\n';
	for(auto i : ellipsoid_v)
	{
		std::cout << '\t'
		<< "(" << i.x << "," << i.y << "," << i.z << "," << i.rx << "," << i.ry << "," << i.rz << ")" << '\n';
	}



	
	/*
	float distance = 1.0; // default
	float aspect=1; // default
	vec3 uprime; // consider moving this to temp value in local scope of where u is computed
	vec3 u;
	vec3 vprime; // consider moving this to temp value in local scope of where u is computed
	vec3 v;
	vec3 n;
*/

	// viewing window
	std::cout << "fovv: " << fovv << '\n';
	// fovh;
	// view_w;
	// view_h;	
	// ul,ur,ll,lr;	

/*
	// map from viewing window to image pixels
	vec3 delta_h;
	vec3 delta_v;

	
	int max_color = 255; // 0 to 256-1
	std::vector<int> pixel_values; // Container for our image data - 3 rgb values (in 0-max_color range) per pixel

	std::string comment = "Assignment 1a";
	*/
}




















#endif
