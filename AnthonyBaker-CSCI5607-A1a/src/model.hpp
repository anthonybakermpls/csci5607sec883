#ifndef MODEL_HPP
#define MODEL_HPP


#include "vec.hpp"

// This is simple now but is here in case we need to do ever more 
// complex shapes and stuff


struct Sphere 
{
	float x=0;
	float y=0;
	float z=0;
	float r=0;
	vec3 mat_color;
};

struct Ellipsoid 
{
	float x=0;
	float y=0;
	float z=0;
	float rx=0;
	float ry=0;
	float rz=0;
	vec3 mat_color;	
};


struct Ray 
{
	vec3 origin;
	vec3 direction;
};


#endif
