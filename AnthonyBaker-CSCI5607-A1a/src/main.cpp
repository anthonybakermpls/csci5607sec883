
/**
 * This program (executable name: assignment1a) reads in an ascii text file 
 * which contains a specified input consisting of a keywords specifing 
 * various properties of the viewing space and output image.
 * The program then generates and writes a file in the required 
 * PPM format with the dimensions as read in from the input file.
 *
 * The basic steps are:
 * - define viewing parameters
 * - define the scene
 * - define equation of each ray and
 * - determine if/where a ray intersects an object
 * 
 * Structure the program for extensibility:
 * 	This part is always hard because you can't predict what enhancements
 * 	will be required in the future which impact the overall structure.
 *  My method is to keep everything "inline" in the main program until 
 *  it becomes huge or unwieldly at which point those methods can be 
 *  refactored into modules.
 * 
 * Usage:
 * ./assignment1a inputfile.txt
 * 
 * 
 * TODO: exceptions, error output conform to RFC syslog
 * Too many parm_objs. Need to make a decision about moving parm_obj into an entity class of some kind
 * distance is whatever we want it to be
 * Notes: You may notice I use much parentheses in expressions.
 */




// std
#include <chrono>
#include <complex>
#include <cmath>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <random> // random number device
#include <vector>

// project
#include "input_file_parser.hpp"
#include "parm_obj.hpp"
#include "model.hpp"




#define PI 3.14159265
//*PI/180.0


// Function declarations
void compute_viewing_parms(Parm_obj&);
void compute_pixels(Parm_obj&);
void check_intersections(Parm_obj& parm_obj, const int& i, const int& j);
void write_output(const std::string&, const Parm_obj& );
int denormalize_color(const float& , const int& );



int main( int argc, char **argv )
{

	// Make sure the user specified an input file. If not, tell them how to do so.
	if( argc < 2 )
	{ 
		std::cerr << "**Error: you must specify an input file, " <<
		"e.g. \"./example ../examplefile.txt\"" << std::endl; 
		return 1; 
	}
	// Get the input text file, which will be the second argument.
	std::string input_filename=argv[1];
	std::cout << "Input file: " << input_filename << std::endl;
	


	Parm_obj parm_obj; // Contains many of our variables and is used for passing through functions. Refactor when necessary.
	Input_file_parser input_file_parser; // Handles reading input files and filling parameters
	parm_obj.print(); // optional step	

	
	// User input functions are good candidates for c++ exceptions so i've read...
	// TODO: add throws
	try
	{
		input_file_parser.read_input(input_filename, parm_obj); // Read input file
	} 
	catch (std::exception& e) 
	{
		std::cerr << "Exception: " << e.what() << std::endl;
		exit(EXIT_FAILURE);
	}
	catch (const std::string& s) 
	{
		std::cerr << "Exception: " << s << std::endl;
		exit(EXIT_FAILURE);
	}
    catch(...) // Catch anything else.
    {
		std::cerr << "General exception." << std::endl;
		exit(EXIT_FAILURE);
    }
    
    

	compute_viewing_parms(parm_obj);
	// parm_obj.print(); // optional step	
	
	compute_pixels(parm_obj);
	

	// filename is something like blah.txt and we want it to be blah.ppm
	std::string output_filename = input_filename.substr(0, input_filename.size() - 4) + ".ppm";
	write_output(output_filename, parm_obj);


	return 0;
}




















/** 
 * This function takes care of setting up all viewing parameters that are
 * not given in our input file
 **/
void compute_viewing_parms(Parm_obj& parm_obj)
{
	// check viewdir and updir are not parallel.
	// exit gracefully
	
	// compute camera frame

	// compute u
	vec3 uprime= cross(parm_obj.view_dir,parm_obj.up_dir);
	parm_obj.u=norm(uprime);	
	
	// compute v
	vec3 vprime= cross(parm_obj.u, parm_obj.view_dir);
	parm_obj.v=norm(vprime);

	// compute n
	parm_obj.n= norm(parm_obj.view_dir);

	// compute w
	parm_obj.w= parm_obj.n*(-1);

	// compute viewing window w,h in world coordinate units
	//parm_obj.view_w = 2*distance* (tan(fovh/2)); // do this below based off of h and aspect
	parm_obj.view_h = 2*parm_obj.distance* ( tan( (parm_obj.fovv*PI/180.0)/2 ) );	// CMATH TAN IS IN RADIANS
	
	// compute aspect ratio given image/view window size
	parm_obj.aspect= parm_obj.imsize_w/parm_obj.imsize_h;
	
	// find viewing window  width from viewing window h and aspect:
	parm_obj.view_w = parm_obj.view_h*parm_obj.aspect;
	
	// compute corners of viewing window in world coordinate space	
	parm_obj.ul =  parm_obj.eye + (parm_obj.n*parm_obj.distance) + (parm_obj.v*(parm_obj.view_h/2)) - (parm_obj.u*(parm_obj.view_w/2));
	parm_obj.ur =  parm_obj.eye + (parm_obj.n*parm_obj.distance) + (parm_obj.v*(parm_obj.view_h/2)) + (parm_obj.u*(parm_obj.view_w/2));
	parm_obj.ll =  parm_obj.eye + (parm_obj.n*parm_obj.distance) - (parm_obj.v*(parm_obj.view_h/2)) - (parm_obj.u*(parm_obj.view_w/2));
	parm_obj.lr =  parm_obj.eye + (parm_obj.n*parm_obj.distance) - (parm_obj.v*(parm_obj.view_h/2)) + (parm_obj.u*(parm_obj.view_w/2));

	// compute delta  h,v (map from image size to view window)
	parm_obj.delta_h = (parm_obj.ur-parm_obj.ul) / (parm_obj.imsize_w -1); // why -1?
	parm_obj.delta_v = (parm_obj.ul-parm_obj.ll) / (parm_obj.imsize_h -1);
	parm_obj.deltaf_h = (parm_obj.ur.x -parm_obj.ul.x) / (parm_obj.imsize_w); // book method
	parm_obj.deltaf_v = (parm_obj.ul.z -parm_obj.ll.z) / (parm_obj.imsize_h); // book method
}




/**
 * for each pixel
 * Compute ray equation
 * compute viewing ray
 * find 1st object hit by ray and its surface normal n
 * set pixel color to value computed from hit point, light, and n
 */
void compute_pixels(Parm_obj& parm_obj)
{

	// push back the full image worth of rgb zeros
	for(int i=0; i< parm_obj.imsize_w*parm_obj.imsize_h; i++)// 3 rgb values (in 0-max_color range) per pixel
	{ 
		parm_obj.pixel_values.push_back( denormalize_color(parm_obj.bg_color.at(0) , parm_obj.max_color ) ); 
		parm_obj.pixel_values.push_back( denormalize_color(parm_obj.bg_color.at(1) , parm_obj.max_color ) ); 
		parm_obj.pixel_values.push_back( denormalize_color(parm_obj.bg_color.at(2) , parm_obj.max_color ) ); 
	}
	
	// use i as horizontal image pixel dimension and j as vertical image pixel dimension
	// but in row-major order so..
	for(int j=0; j<parm_obj.imsize_h; j++) //  move down
	{	
		for (int i =0; i<parm_obj.imsize_w; i++) //  move across
		{
			check_intersections(parm_obj,i,j); // do the ray/object intersection and pixel value storage
		}
	}

}



/**
 * For each object in our scene, check to see if the given ray intersects it.
 * TODO: Refactor this.
 */
void check_intersections(Parm_obj& parm_obj, const int& i, const int& j)
{
	
	Ray ray;
	ray.origin = parm_obj.eye;	
	 	
	float p_x=0;
	float p_z=0; 
	vec3 p;
	vec3 p_minus_e;

			
	float A=0.0;
	float B=0.0;
	float C=0.0;
	float discriminant=0;
	
	float t1=0; // solved for parameter
	float t2=0; // solved for parameter
	float closer_t=0; // for picking one of two solutions
	
	
	float vec_element=0; // used for std::vector element location
	 
	 
	float min_t=99999; // this is the minimum t out of all "hits" we will overwrite on the pixel if current t is less than this
	 
	//p= parm_obj.ul + parm_obj.delta_v*i + parm_obj.delta_h*j;		

	// The following is how the book does it
	p_x = parm_obj.ul.x + parm_obj.deltaf_h * (i+0.5);
	p_z = parm_obj.ll.z + parm_obj.deltaf_v * (j+0.5);
	p.x = p_x;
	p.y = parm_obj.distance;
	p.z = p_z;

	// ray equation is r(t) = e + t(p - e)
	// (p-e)/norm(p-e)
	p_minus_e = p - parm_obj.eye;
	ray.direction= (p_minus_e)/length(p_minus_e); // normalize ray by dividing by length
	

	// we are at pixel i,j. This translates to location in a serial vector element list of
	vec_element = (j*parm_obj.imsize_w + i )* 3; // multiply by three because we have 3 values per pixel
	vec_element = (j*parm_obj.imsize_w + i )* 3; // multiply by three because we have 3 values per pixel

		
	// check all spheres in scene
	for( auto current_sphere : parm_obj.sphere_v)
	{

		// A = std::pow(ray.direction.x,2) + std::pow(ray.direction.y,2) + std::pow(ray.direction.z,2) ;
		// Because this is normalized, A=1  // Don't use this if ray is not normalized.
		A = 1;
		B = 2*( ray.direction.x* (ray.origin.x - current_sphere.x)
			+   ray.direction.y* (ray.origin.y - current_sphere.y)
			+   ray.direction.z* (ray.origin.z - current_sphere.z) );
		C = pow(ray.origin.x-current_sphere.x,2)
			+ pow(ray.origin.y-current_sphere.y,2)
			+ pow(ray.origin.z-current_sphere.z,2)
			- pow(current_sphere.r,2);
		
		// compute discriminant B^2 - 4AC
		discriminant = B*B - 4*A*C;

		if ( discriminant < 0 ) // no solution
		{
			// NOoP
		}
		else if ( discriminant == 0 )	// 1 solution
		{
			// parameter value of intersection
			closer_t = (-B)/(2*A); // discriminant zero
				
			if(closer_t < min_t)// this is the minimum t out of all "hits" we will overwrite on the pixel if current t is less than this
			{
				// need to denormalize our colors to parm_obj.max_color	
				parm_obj.pixel_values.at(vec_element) =  denormalize_color(current_sphere.mat_color.x,parm_obj.max_color);
				parm_obj.pixel_values.at(vec_element+1) =  denormalize_color(current_sphere.mat_color.y,parm_obj.max_color);
				parm_obj.pixel_values.at(vec_element+2) =  denormalize_color(current_sphere.mat_color.z,parm_obj.max_color);									
				min_t =closer_t;
			}
		}
		else if ( discriminant > 0 ) // two solutions - Fix later to figure out which one
		{
			// Which t value is closer to eye?
			t1 = (-B + sqrt(discriminant))/(2*A);			
			t2 = (-B - sqrt(discriminant))/(2*A);
			
			if(t1 >0 && t1<t2){closer_t = t1;}
			else if(t2>0 && t2<t1){closer_t=t2;}
				
			if(closer_t < min_t)// this is the minimum t out of all "hits" we will overwrite on the pixel if current t is less than this
			{
				// need to denormalize our colors to parm_obj.max_color		
				parm_obj.pixel_values.at(vec_element) =  denormalize_color(current_sphere.mat_color.x,parm_obj.max_color);
				parm_obj.pixel_values.at(vec_element+1) =  denormalize_color(current_sphere.mat_color.y,parm_obj.max_color);
				parm_obj.pixel_values.at(vec_element+2) =  denormalize_color(current_sphere.mat_color.z,parm_obj.max_color);								
				min_t =t1;
			}			
									
		}
	}



	// check all Ellipsoids in scene
	for( auto current_ellipsoid : parm_obj.ellipsoid_v)
	{

		A = std::pow(ray.direction.x,2)/current_ellipsoid.rx + std::pow(ray.direction.y,2)/current_ellipsoid.ry + std::pow(ray.direction.z,2)/current_ellipsoid.rz ;
		// Because this is normalized, A=1  // Don't use this if ray is not normalized.
		//A = 1;
		B = 2*( ray.direction.x* (ray.origin.x - current_ellipsoid.x)/current_ellipsoid.rx
			+   ray.direction.y* (ray.origin.y - current_ellipsoid.y)/current_ellipsoid.ry
			+   ray.direction.z* (ray.origin.z - current_ellipsoid.z)/current_ellipsoid.rz );
		C = pow(ray.origin.x-current_ellipsoid.x,2)/current_ellipsoid.rx
			+ pow(ray.origin.y-current_ellipsoid.y,2)/current_ellipsoid.ry
			+ pow(ray.origin.z-current_ellipsoid.z,2)/current_ellipsoid.rz
			- 1;
		
		// compute discriminant B^2 - 4AC
		discriminant = B*B - 4*A*C;



		if ( discriminant < 0 ) // no solution
		{
			// NOoP
		}
		else if ( discriminant == 0 )	// 1 solution
		{

			// parameter value of intersection
			closer_t = (-B)/(2*A); // discriminant zero
			
			if(closer_t < min_t)// this is the minimum t out of all "hits" we will overwrite on the pixel if current t is less than this
			{		
				// need to denormalize our colors to parm_obj.max_color	
				parm_obj.pixel_values.at(vec_element) =  denormalize_color(current_ellipsoid.mat_color.x,parm_obj.max_color);
				parm_obj.pixel_values.at(vec_element+1) =  denormalize_color(current_ellipsoid.mat_color.y,parm_obj.max_color);
				parm_obj.pixel_values.at(vec_element+2) =  denormalize_color(current_ellipsoid.mat_color.z,parm_obj.max_color);									
				min_t =closer_t;
			}					
		}
		else if ( discriminant > 0 ) // two solutions - Fix later to figure out which one
		{
			// parameter value of intersection
			// Which t value is closer to eye?
			t1 = (-B + sqrt(discriminant))/(2*A);
			t2 = (-B - sqrt(discriminant))/(2*A);

			if(t1 >0 && t1<t2){closer_t = t1;}
			else if(t2>0 && t2<t1){closer_t=t2;}		
			
			if(closer_t < min_t)// this is the minimum t out of all "hits" we will overwrite on the pixel if current t is less than this
			{			
				// need to denormalize our colors to parm_obj.max_color		
				parm_obj.pixel_values.at(vec_element) =  denormalize_color(current_ellipsoid.mat_color.x,parm_obj.max_color);
				parm_obj.pixel_values.at(vec_element+1) =  denormalize_color(current_ellipsoid.mat_color.y,parm_obj.max_color);
				parm_obj.pixel_values.at(vec_element+2) =  denormalize_color(current_ellipsoid.mat_color.z,parm_obj.max_color);	
				min_t =closer_t;
			}								
		}
	}



}



/**
 * Write the ouptut image file
 */
void write_output(const std::string& filename, const Parm_obj& parm_obj)
{
	// build a PPM header
	// Format of our input file is keyword "imgsize" and then two integers specifiying the dimension
	// Format of output is a magic number, comment, dimensions, color range, and then our pixel values in RGB tuples.	
	std::string magic_number = "P3";

	std::string ppm_header(
	magic_number+"\n"
	+parm_obj.comment+"\n"
	+std::to_string(parm_obj.imsize_w)+" "+std::to_string(parm_obj.imsize_h)+"\n"
	+std::to_string(parm_obj.max_color)+"\n"
	);

	// Create a filestream for outputting the text file, and open it. Quit program if there's an error
	std::ofstream outfile( filename.c_str() );
	if( !outfile.is_open() )
	{ std::cerr << "**Error: Could not open output file " << filename << std::endl; exit(1); }
	
	// Write the header
	outfile << ppm_header; // last item already has a newline


	// Write the pixel RGB values
	std::cout << "Writing " << filename << '\n';
	int line_length=0; // allow us to write more than one value per line
	for(auto i : parm_obj.pixel_values)
	{
		outfile << i;
		line_length++;
		if(line_length >14)    // 3 digts + 1 space * 3 components = 12 bytes max size of one pixel (5 pixels * 12 bytes = 60)
		{
			line_length=0;
			outfile << '\n';
		}
		else{
			outfile << " ";
		}
	}

}



int denormalize_color(const float& f, const int& max_denorm_color )
{
	return floor(f == 1.0 ? max_denorm_color : (int) (f * (float)(max_denorm_color + 1)) );
}




