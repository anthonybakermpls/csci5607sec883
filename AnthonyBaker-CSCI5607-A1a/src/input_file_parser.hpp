#ifndef INPUT_FILE_PARSER_HPP
#define INPUT_FILE_PARSER_HPP





/**
 * class Input_file_parser
 * 
 * At some point we can make this super generic that can handle files 
 * based on a file extension or a spec or something.
 */





#include <string>
#include "parm_obj.hpp"
#include "vec.hpp"



class Input_file_parser
{
public:
	Input_file_parser(){}
	void read_input(const std::string& , Parm_obj&);
};





void Input_file_parser::read_input(const std::string& filename, Parm_obj& parm_obj)
{

	// Are there any input file name requirements? I guess we can at least check the extension.
	if (filename.size() < 5)
	{
		std::cerr << "Error: malformed file name. " << std::endl; 
		 exit(1);
	}

	std::string ext = filename.substr(filename.size() -4);

	if ( ext != ".txt") 
	{
		std::cerr << "Error: incorrect file extension. " << std::endl; 
		 exit(1);
	}

	// Create a filestream for reading the text file, and open it. Quit program if there's an error
	std::ifstream inputfile_ifs;	
	inputfile_ifs.open( filename.c_str() );
	if( !inputfile_ifs.is_open() )
	{ std::cerr << "**Error: Could not open file " << filename << std::endl; exit(1); }




	std::string var="";
		
	/* Assignemt 1 a input format
	 * eye float float float
	 * viewdir float float float
	 * updir float float float
	 * fovv float
	 * imsize int int
	 * bkgcolor float float float // in [0,1]
	 * mtlcolor float float float // in [0,1]
	 * sphere float float float float
	 * ellipsoid float float float float float float
	 */




	// Read input file and try to handle bad cases
	
	std::string line="";
	
	// do one getline and if we see eof then the file is empty.
	std::getline( inputfile_ifs, line );
	if(inputfile_ifs.eof())
	{ 
		std::cerr << "**Error: File empty." << filename << std::endl; 
		exit(1); 
	}
	else  // clear eof bit and rewind
	{
		inputfile_ifs.clear();
		inputfile_ifs.seekg(0, inputfile_ifs.beg);
	}
	
	vec3 mat_color; // a local copy that will be used for every shape and only changed when a new mtlcolor line is found
	while( std::getline( inputfile_ifs, line ) )
	{
	
		std::stringstream ss( line );
		ss >> var;
		
		if( var == "eye" )
		{
			ss >> parm_obj.eye.x; 
			ss >> parm_obj.eye.y; 
			ss >> parm_obj.eye.z; 	
			
		}
		if( var == "viewdir" )
		{
			ss >> parm_obj.view_dir.x; 
			ss >> parm_obj.view_dir.y; 
			ss >> parm_obj.view_dir.z; 	
			if( 
				(length(parm_obj.view_dir) != 1)
			)
			{ std::cerr << "Error: input vector not unit length: " << "viewdir" << std::endl; exit(1); }						
		}

		if( var == "updir" )
		{
			ss >> parm_obj.up_dir.x; 
			ss >> parm_obj.up_dir.y; 
			ss >> parm_obj.up_dir.z; 	
			if( 
				(length(parm_obj.up_dir) != 1)
			)
			{ std::cerr << "Error: input vector not unit length: " << "updir" << std::endl; exit(1); }	
			
			vec3 temp=cross(parm_obj.view_dir,parm_obj.up_dir);
			if( (temp.x==0) && (temp.y==0) && (temp.z==0) )
			{ std::cerr << "Error: input view and up vectors are parallel." << std::endl; exit(1); }	
		}

		
		if( var == "fovv" )
		{
			ss >> parm_obj.fovv; 
			if( 
				(parm_obj.fovv <0) || (parm_obj.fovv >180)
			)
			{ std::cerr << "Error: fovv out of range" << filename << std::endl; exit(1); }				
		}

		if( var == "imsize" )
		{
			ss >> parm_obj.imsize_w >> parm_obj.imsize_h;
			if( 
				(parm_obj.imsize_w <1) || (parm_obj.imsize_w <1)
			)
			{ std::cerr << "Error: image size out of range" << filename << std::endl; exit(1); }				
		}

		if( var == "bkgcolor" )
		{
			float temp;
			ss >> temp;
				
			if( 
				(temp <0) || (temp >1)
			)
			{ std::cerr << "Error: bkgcolor out of range" << filename << std::endl; exit(1); }	
						
			parm_obj.bg_color.push_back(temp); 
			ss >> temp;
			parm_obj.bg_color.push_back(temp);
			ss >> temp;
			parm_obj.bg_color.push_back(temp);				
		}

		
		// This keyword lines can apppear any number of times and sets the color for objects after until
		// another mtlcolor is called.
		if( var == "mtlcolor" ) // only change the local copy
		{
			float temp;
			ss >> temp;
				
			if( 
				(temp <0) || (temp >1)
			)
			{ std::cerr << "Error: mtlcolor out of range" << filename << std::endl; exit(1); }	
							
			mat_color.x = temp;
			ss >> temp;
			mat_color.y = temp;
			ss >> temp;
			mat_color.z = temp;			
		}
				
		
		
		// These keyword lines can apppear any number of times
		if( var == "sphere" )
		{
			Sphere s;
			ss >> s.x >> s.y >> s.z >> s.r;
			s.mat_color = mat_color; // set to the current "state" mat color
			parm_obj.sphere_v.push_back(s);
		}
				
		if( var == "ellipsoid" )
		{
			Ellipsoid e;
			ss >> e.x >> e.y >> e.z >> e.rx >> e.ry >> e.rz;
			e.mat_color = mat_color; // set to the current "state" mat color
			parm_obj.ellipsoid_v.push_back(e);
		}

	}// end while getline loop
	
	if (inputfile_ifs.bad())
	{ 
		std::cerr << "**Error: Read/writing error on i/o operation. " << filename << std::endl; 
		exit(1); 
	}
	//else if(inputfile_ifs.fail())
	//{ 
	//	std::cerr << "**Error: Logical error on i/o operation. " << filename << std::endl; 
	//	exit(1); 
	//}
		

}



#endif
