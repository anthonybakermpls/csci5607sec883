#ifndef VEC_HPP
#define VEC_HPP






/**
 * class vec2 and vec3
 * 
 * Has basic routines and overloaded operators.
 * 
 */







#include <sstream>










class vec2
{
public:
	vec2() : x(0), y(0) {}
	vec2(const float& x_,const float& y_ ) : x(x_), y(y_) {}
	float x, y;
	vec2& operator = (const vec2& other);
	vec2 operator + (const vec2& other);	
	vec2 operator - (const vec2& other);	
	vec2 operator * (const float& scalar);	
	vec2 operator / (const float& scalar);		
	std::string to_str();
};



class vec3 
{
public:
	vec3() : x(0), y(0), z(0) {}
	vec3(const float& x_,const float& y_, const float& z_ ) : x(x_), y(y_), z(z_) {}
	float x, y, z;
	vec3& operator = (const vec3& other);
	vec3 operator + (const vec3& other);		
	vec3 operator - (const vec3& other);			
	vec3 operator * (const float& scalar);	
	vec3 operator / (const float& scalar);	
	std::string to_str();
};


vec2& vec2::operator = (const vec2& other)
{
	if (this != &other) // protect against invalid self-assignment
	{
		this->x=other.x;
		this->y=other.y;
	}

	return *this;
}

vec2 vec2::operator + (const vec2& other)
{
	vec2 temp;
	temp.x = this->x+other.x;
	temp.y = this->y+other.y;
	return temp;
}

vec2 vec2::operator - (const vec2& other)
{
	vec2 temp;
	temp.x = this->x-other.x;
	temp.y = this->y-other.y;
	return temp;
}

vec2 vec2::operator * (const float& scalar)
{
	vec2 temp;
	temp.x = this->x *scalar;
	temp.y = this->y *scalar;
	return temp;
}

vec2 vec2::operator / (const float& scalar)
{
	vec2 temp;
	temp.x = this->x /scalar;
	temp.y = this->y /scalar;
	return temp;
}

    
std::string vec2::to_str()
{
	std::stringstream ss;
	ss << '(' << x << ',' << y << ')';
	return ss.str();
}








vec3& vec3::operator = (const vec3& other)
{
	if (this != &other) // protect against invalid self-assignment
	{
		this->x=other.x;
		this->y=other.y;
		this->z=other.z;		
	}
	return *this;
}
    

vec3 vec3::operator + (const vec3& other)
{
	vec3 temp;
	temp.x = this->x+other.x;
	temp.y = this->y+other.y;
	temp.z = this->z+other.z;	
	return temp;
}

vec3 vec3::operator - (const vec3& other)
{
	vec3 temp;
	temp.x = this->x-other.x;
	temp.y = this->y-other.y;
	temp.z = this->z-other.z;	
	return temp;
}

vec3 vec3::operator * (const float& scalar)
{
	vec3 temp;
	temp.x = this->x*scalar;
	temp.y = this->y*scalar;
	temp.z = this->z*scalar;	
	return temp;
}

vec3 vec3::operator / (const float& scalar)
{
	vec3 temp;
	temp.x = this->x/scalar;
	temp.y = this->y/scalar;
	temp.z = this->z/scalar;	
	return temp;
}


std::string vec3::to_str()
{
	std::stringstream ss;
	ss << '(' << x << ',' << y << ',' << z << ')';
	return ss.str();
}









float dot(const vec2& u, const vec2& v)
{
	float dot = u.x*v.x + u.y*v.y; 
	return dot;
}


float dot(const vec3& u, const vec3& v)
{
	float dot = u.x*v.x + u.y*v.y; 
	return dot;	
}


vec2 cross(const vec2& u, const vec2& v)
{
	vec2 w;
	w.x = u.x*v.y - v.x*u.y; // double check this
	return w;
}


vec3 cross(const vec3& u, const vec3& v)
{
	vec3 w;
	w.x = (u.y*v.z) - (v.y*u.z);
	w.y = (u.z*v.x) - (v.z*u.x);
	w.z = (u.x*v.y) - (v.x*u.y);
	return w;
}


vec3 norm(const vec3& u)
{
	float length= sqrt( (u.x*u.x) + (u.y*u.y) + (u.z*u.z) );
	vec3 n=u;
	n=n/length;
	return n;
}


float length(const vec2& u)
{
	return sqrt((u.x*u.x) + (u.y*u.y));
}

float length(const vec3& u)
{
	return sqrt((u.x*u.x) + (u.y*u.y) + (u.z*u.z));
}
















#endif
