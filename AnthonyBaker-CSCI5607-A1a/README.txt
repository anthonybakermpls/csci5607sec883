﻿Anthony Baker
CSCI 5607 SEC 883

Assignment 1a: Getting Started With Raycasting


This program (executable name: assignment1a) reads in an ascii text file which contains specified input consisting of keywords to define a scene for raycasting. The program then generates and writes a file in the required PPM format with the dimensions as read in from the input file.

To build:
mkdir build
cd build
cmake ..


Usage:
./assignment1a inputfile.txt



