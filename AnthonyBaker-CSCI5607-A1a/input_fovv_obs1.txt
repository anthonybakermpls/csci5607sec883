eye 0 0 0
viewdir 0 1 0
updir 0 0 1
fovv 180.0
imsize 400 400
bkgcolor 0.3 0.3 0.3
mtlcolor 0.5 0.0 1.0
ellipsoid -0.1 0.5 0 0.005 0.001 0.001
mtlcolor 1.0 0.0 0.0
sphere 0.1 0.5 0 0.01
mtlcolor 0.0 1.0 0.0
sphere -0.9 1 0.9 0.04
sphere -0.8 1 0.9 0.04
sphere -0.7 1 0.9 0.04
sphere -0.6 1 0.9 0.04
sphere -0.5 1 0.9 0.04
sphere -0.4 1 0.9 0.04
sphere -0.3 1 0.9 0.04
sphere -0.2 1 0.9 0.04
sphere -0.1 1 0.9 0.04
mtlcolor 1.0 1.0 0.0
sphere -0.9 1 0.9 0.04
sphere -0.9 1 0.8 0.04
sphere -0.9 1 0.7 0.04
sphere -0.9 1 0.6 0.04
sphere -0.9 1 0.5 0.04
sphere -0.9 1 0.4 0.04
sphere -0.9 1 0.3 0.04
sphere -0.9 1 0.2 0.04
sphere -0.9 1 0.1 0.04
mtlcolor 0.0 0.0 1.0
#top to bottom
sphere 0.0 0.0 -0.9 0.04
sphere 0.0 0.1 -0.8 0.04
sphere 0.0 0.2 -0.7 0.04
sphere 0.0 0.3 -0.6 0.04
sphere 0.0 0.4 -0.5 0.04
sphere 0.0 0.5 -0.4 0.04
sphere 0.0 0.6 -0.3 0.04
sphere 0.0 0.7 -0.2 0.04
sphere 0.0 0.8 -0.1 0.04
sphere 0.0 0.9 0.0 0.04
sphere 0.0 1.0 0.1 0.04
sphere 0.0 1.1 0.2 0.04
sphere 0.0 1.2 0.3 0.04
sphere 0.0 1.3 0.4 0.04
sphere 0.0 1.4 0.5 0.04
sphere 0.0 1.5 0.6 0.04
sphere 0.0 1.6 0.7 0.04
sphere 0.0 1.7 0.8 0.04
sphere 0.0 1.8 0.9 0.04
