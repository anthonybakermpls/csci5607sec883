
/*
 
This program (executable name: assignment0) reads in an ascii text file 
which contains a specified input consisting of a keyword "imsize" and 
then two integer values corresponding to the dimensions of an output 
image file. The program then generates and writes a file in the required 
PPM format with the dimensions as read in from the input file.

Usage:
./assignment0 inputfile.txt

*/




// std
#include <chrono>
#include <complex>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <random> // random number device
#include <vector>


// Declare image generation functions
void static_colors(const int&, const int&, std::vector<int>&, const int& max_color);
void random_colors(const int&, const int&, std::vector<int>&, const int& max_color);
void fractal(const int&, const int&, std::vector<int>&, const int& max_color);










int main( int argc, char **argv )
{






	int imsize_w=0, imsize_h=0;	







	// Make sure the user specified an input file. If not, tell them how to do so.
	if( argc < 2 )
	{ 
		std::cerr << "**Error: you must specify an input file, " <<
		"e.g. \"./example ../examplefile.txt\"" << std::endl; return 0; 
	}

	// Get the input text file, which will be the second argument.
	std::string input_filename=argv[1];
	std::cout << "Input file: " << input_filename << std::endl;
	
	// Are there any input file name requirements? I guess we can at least check the extension.
	if ( (input_filename.size() < 5) || ( input_filename.substr(input_filename.size() - 4) != ".txt") )
	{std::cerr << "Error: incorrect file extension. Expected : " << "\".txt\"" << std::endl; return 0;}





	// Create a filestream for reading the text file, and open it. Quit program if there's an error
	std::ifstream inputfile_ifs;	
	inputfile_ifs.open( input_filename.c_str() );
	if( !inputfile_ifs.is_open() )
	{ std::cerr << "**Error: Could not open file " << input_filename << std::endl; return -1; }

	// Read input file and try to handle bad cases
	std::string line="";
	while( std::getline( inputfile_ifs, line ) )
	{
		if(line=="") // check empty file
		{std::cerr << "Input syntax incorrect. Empty line." << std::endl; return -1;}

		std::stringstream ss( line );
		std::string var="";
		ss >> var;
		if( var == "imsize" ) // Our required keyword
			ss >> imsize_w >> imsize_h; // Anything after this is ignored 
		else { std::cerr << "Input syntax incorrect. Keyword not found." << var << std::endl;  return -1;}
		
		 // imsize constraints
		if( 
			(imsize_w == 0)
			|| (imsize_h == 0)
			|| (imsize_w < 0) 
			|| (imsize_h < 0)
			|| (imsize_w > 4000) // 4k enough? 
			|| (imsize_h > 4000)
		)
		{ std::cerr << "Input syntax incorrect. Failed image dimensions. " << std::endl;  return -1; }		
	}

	
	std::cout << "Image size: " << "width: (" << imsize_w << ") height: (" << imsize_h << ")" << std::endl;
	



	// build a PPM header
	// Format of our input file is keyword "imgsize" and then two integers specifiying the dimension
	// Format of output is a magic number, comment, dimensions, color range, and then our pixel values in RGB tuples.	
	std::string magic_number = "P3";
	std::string comment = "# CSCI 5607 (883) Assignment 0";
	int max_color = 255; // 0 to 256-1
	
	std::string ppm_header(
	magic_number+"\n"
	+comment+"\n"
	+std::to_string(imsize_w)+" "+std::to_string(imsize_h)+"\n"
	+std::to_string(max_color)+"\n"
	);



	// Container for our image data
	std::vector<int> pixel_values; // 3 rgb values (in 0-max_color range) per pixel

	// Generate our pixel colors
	fractal(imsize_w, imsize_h, pixel_values,max_color);




	// filename is something like blah.txt and we want it to be blah.ppm
	std::string outfilename = input_filename.substr(0, input_filename.size() - 4);

	// Create a filestream for outputting the text file, and open it. Quit program if there's an error
	std::ofstream outfile( (outfilename+".ppm").c_str() );
	if( !outfile.is_open() )
	{ std::cerr << "**Error: Could not open output file " << outfilename << std::endl; return 0; }
	
	// Write the header
	outfile << ppm_header; // last item already has a newline

	// Write the pixel RGB values
	std::cout << "Writing pixel values..." << '\n';
	int line_length=0; // allow us to write more than one value per line
	for(auto i : pixel_values)
	{
		outfile << i;
		line_length++;
		if(line_length >15)    // 3 digts + 1 space * 3 components = 12 bytes max size of one pixel (5 pixels * 12 bytes = 60)
		{
			line_length=0;
			outfile << '\n';
		}
		else{
			outfile << " ";
		}
	}





	return 0;
}












// Output routines



// Static color to all pixels
void static_colors(const int &w, const int &h, std::vector<int>& pixel_values)
{
	for(int col =0; col< w; col++)
	{
		for(int row=0; row< h; row++)
		{
			// RGB components for each pixel
			pixel_values.push_back(0);
			pixel_values.push_back(0);
			pixel_values.push_back(0);
		}
	}
}

// Random colours
void random_colors(const int &w, const int &h, std::vector<int>& pixel_values)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(0, 255);	

	// Assign a color to all pixels
	for(int col =0; col<= w; col++)
	{
		for(int row=0; row<= h; row++)
		{
			// RGB components for each pixel
			//pixel_values.push_back(0);
			//pixel_values.push_back(0);
			//pixel_values.push_back(0);
			pixel_values.push_back(dist(mt));
			pixel_values.push_back(dist(mt));
			pixel_values.push_back(dist(mt));
		}
	}
}


// Fractal
void fractal(const int &w, const int &h, std::vector<int>& pixel_values, const int& max_color)
{




	// create a mapping from [x,y] to [a,b] for our image size to mandelbrot radius
	// I had to look this up because i thought i could use a scalar and that didn't work
	// (val - x)*(b-a)/(y-x) + a
	int a=-2;
	int b=2;
	float b_minus_a = b-a;

    float x=0;
    float y=0;
    float epsilon = 1; //0.01; // The step size across the X and Y axis

    int max_iterations = 1000; // increasing this will give you a more detailed fractal

    std::complex<double> Z;
    std::complex<double> C;
	float r=0; // temp real value
	float im=0; // temp imaginary value

    int iterations=0;
    
	float red = 0;
	float green=0;
	float blue=0;


//	bool first_calc=1; // for time estimation code
	
    for(y=0; y<h; y+= epsilon)  // for each row
    {
		//std::cout << "Computing next row: " << y << '\n';
        for(x=0; x<w; x+= epsilon) //  each column
        {
//			if(first_calc==1)
//				auto start = chrono::steady_clock::now();
			
			//std::cout << "Computing next column: " << x << '\n';
            iterations = 0;
			r=(x)*(b_minus_a)/(w)+a;
			im=(y)*(b_minus_a)/(h)+a;
            C = std::complex<double>(r,im);
            Z = std::complex<double>(0,0);
            while( (std::abs(Z) < 2) && (iterations < max_iterations) )
            {
                Z = Z*Z + C;
                iterations++;
            }

			// messing around with color schemes
			//red=((iterations*-max_color)/max_iterations) +max_color; // linear mapping'
			//green= 200*( 1/(1+exp(-1*iterations)) ); // sigmoid
			//blue=255*log10(iterations); // log mapping
			//blue = ((iterations-0)*(max_color))/(max_iterations);   // linear mapping from [0,max_iterations] to [0,max_color]
			
			red= ( (iterations*(-max_color))/max_iterations ) +max_color; // linear mapping from [0,max_iterations] to [max_color,0]

			if(iterations>=max_iterations)
			{
				green=0;
				blue=0;
			}
			else
			{
				//green= 200*( 1/(1+exp(-1*iterations)) ); // sigmoid;
				blue=255*log10(iterations); // log mapping 
			}

			// Store RGB components for each pixel
			pixel_values.push_back( red * 0.4);
			pixel_values.push_back( green ); 
			pixel_values.push_back( blue ); 
			




/*	TODO : understand std::chrono
			if(first_calc==1)
			{
				auto end = chrono::steady_clock::now();			
				auto diff = end - start;
				cout << "Estimated time remaining: " << (chrono::duration <double, std::ratio<60>> (diff).count()) *(epsilon*w)*(epsilon*h) << " min" << endl;
				first_calc=0;
			}
*/			
        }
    }
}














