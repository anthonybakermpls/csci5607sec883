# ./assignment1d assign1d_0.txt; display assign1d_0.ppm

eye 0.5 0.6 2.5
viewdir 0 0 -1
updir 0 1 0
fovv 45
imsize 800 800
bkgcolor 0.2 0.2 0.3


light 4  5 0 1 1 1 1
light 1 -1 0 0 1 1 1



# Odr Odg Odb Osr Osg Osb  ka kd ks n alpha eta

# REFERENCE GREEN
 mtlcolor 0 1 0 1 1 1   0.2 0.5 0.2 100 1 3
sphere 0.5 0.4 0.5 0.3



mtlcolor 1 1 1 0 0 0 1 1 1 2 1 0
texture check.ppm
# quad
v 0 0 0
v 1 0 0
v 1 0 1
v 0 0 1
vt 1 0
vt 1 1
vt 0 1
vt 0 0
f 1/1 2/2 3/3
f 1/1 3/3 4/4
